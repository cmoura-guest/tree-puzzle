TREE-PUZZLE 

Input file name: qp-vt-prot.prot
Type of analysis: tree reconstruction
Parameter estimation: approximate (faster)
Parameter estimation uses: neighbor-joining tree (for substitution process and rate variation)

Standard errors (S.E.) are obtained by the curvature method.
The upper and lower bounds of an approximate 95% confidence interval
for parameter or branch length x are x-1.96*S.E. and x+1.96*S.E.


SEQUENCE ALIGNMENT

Input data: 7 sequences with 128 amino acid sites
Number of constant sites: 7 (= 5.5% of all sites)
Number of site patterns: 125
Number of constant site patterns: 5 (= 4.0% of all site patterns)


SUBSTITUTION PROCESS

Model of substitution: VT (Mueller-Vingron 2000)
Amino acid frequencies (estimated from data set):

 pi(A) =  12.2%
 pi(R) =   2.3%
 pi(N) =   3.0%
 pi(D) =   4.9%
 pi(C) =   0.7%
 pi(Q) =   2.3%
 pi(E) =   5.4%
 pi(G) =   6.0%
 pi(H) =   5.8%
 pi(I) =   2.7%
 pi(L) =  12.5%
 pi(K) =   9.3%
 pi(M) =   1.3%
 pi(F) =   4.9%
 pi(P) =   2.6%
 pi(S) =   6.7%
 pi(T) =   4.5%
 pi(W) =   1.3%
 pi(Y) =   2.2%
 pi(V) =   9.4%


AMBIGUOUS CHARACTERS IN THE SEQUENCE (SEQUENCES IN INPUT ORDER)

               gaps  wildcards        sum   % sequence
 HBB_HUMAN        0          0          0        0.00%   
 HBB_HORSE        0          0          0        0.00%   
 HBA_HUMAN        0          0          0        0.00%   
 HBA_HORSE        0          0          0        0.00%   
 MYG_PHYCA        0          0          0        0.00%   
 GLB5_PETMA       0          0          0        0.00%   
 LGB2_LUPLU       0          0          0        0.00%   
 -------------------------------------------------------
 Sum              0          0          0        0.00%   


The table above shows the amount of gaps ('-') and other 'wildcard'
characters ('X', '?', etc.) and their percentage of the 128 columns
in the alignment.
Sequences with more than 50% ambiguous characters are marked with a '!' and 
should be checked, whether they have sufficient overlap to other sequences.
Sequences with 100% ambiguous characters do not hold any phylogenetic
information and had to be discarded from the analysis.



SEQUENCE COMPOSITION (SEQUENCES IN INPUT ORDER)

              5% chi-square test  p-value
 HBB_HUMAN         passed          92.31%  
 HBB_HORSE         passed          79.58%  
 HBA_HUMAN         passed          93.09%  
 HBA_HORSE         passed          81.20%  
 MYG_PHYCA         passed          33.23%  
 GLB5_PETMA        passed          19.11%  
 LGB2_LUPLU        passed          41.67%  

The chi-square tests compares the amino acid composition of each sequence
to the frequency distribution assumed in the maximum likelihood model.

WARNING: Result of chi-square test may not be valid because of small
maximum likelihood frequencies and short sequence length!


IDENTICAL SEQUENCES

The sequences in each of the following groups are all identical. To speed
up computation please remove all but one of each group from the data set.

 All sequences are unique.


MAXIMUM LIKELIHOOD DISTANCES

Maximum likelihood distances are computed using the selected model of
substitution and rate heterogeneity.

  7
HBB_HUMAN   0.00000  0.16783  0.85204  0.87347  1.69056  1.50964  2.05575
HBB_HORSE   0.16783  0.00000  0.87363  0.87544  1.67978  1.52916  2.09374
HBA_HUMAN   0.85204  0.87363  0.00000  0.11958  1.58540  1.13053  2.11864
HBA_HORSE   0.87347  0.87544  0.11958  0.00000  1.64085  1.18694  2.01999
MYG_PHYCA   1.69056  1.67978  1.58540  1.64085  0.00000  1.63531  2.09093
GLB5_PETMA  1.50964  1.52916  1.13053  1.18694  1.63531  0.00000  1.84636
LGB2_LUPLU  2.05575  2.09374  2.11864  2.01999  2.09093  1.84636  0.00000

Average distance (over all possible pairs of sequences):  1.40836
                  minimum  : 0.11958,  maximum  : 2.11864
                  variance : 0.36159,  std.dev. : 0.60133


RATE HETEROGENEITY

Model of rate heterogeneity: uniform rate


QUARTET STATISTICS (SEQUENCES IN INPUT ORDER)


 name       | resolved        | partly resolved | unresolved      | sum
 --------------------------------------------------------------------------
 HBB_HUMAN        19 [ 95.00%]       0 [  0.00%]       1 [  5.00%]      20
 HBB_HORSE        19 [ 95.00%]       0 [  0.00%]       1 [  5.00%]      20
 HBA_HUMAN        19 [ 95.00%]       0 [  0.00%]       1 [  5.00%]      20
 HBA_HORSE        19 [ 95.00%]       0 [  0.00%]       1 [  5.00%]      20
 MYG_PHYCA        16 [ 80.00%]       0 [  0.00%]       4 [ 20.00%]      20
 GLB5_PETMA       16 [ 80.00%]       0 [  0.00%]       4 [ 20.00%]      20
 LGB2_LUPLU       16 [ 80.00%]       0 [  0.00%]       4 [ 20.00%]      20
 --------------------------------------------------------------------------
  #quartets :     31 [ 88.57%]       0 [  0.00%]       4 [ 11.43%]      35

The table shows the occurrences of fully resolved, partially, and
completely unresolved quartets for each sequence and their percentage
relative to the number of times the sequence occurs in the list of 
quartets (i.e. 20 quartets out of 35 in total).
In fully resolved quartet one single topology is supported, while for
partially resolved quartets two and for completely unresolved quartets
none of the topologies (AB||CD, AC||BD, AD||BC) are favoured.
Note: Because 4 sequences are involved in one quartet numbers add up
to a four-fold of the existing quartets.

Hint: The overall numbers in the last row give information about the
phylogenetic content of the dataset. The higher the percentage of partially
and unresolved quartets, the lower the content of phylogenetic information.
This can be visualized in more detail by likelihood mapping analysis.



TREE SEARCH

Quartet puzzling is used to choose from the possible tree topologies
and to simultaneously infer support values for internal branches.

Number of puzzling steps: 1000
Analysed quartets: 35
Fully resolved quartets:  31 (= 88.6%)
Partly resolved quartets: 0 (= 0.0%)
Unresolved quartets:      4 (= 11.4%)

Quartet trees are based on approximate maximum likelihood values
using the selected model of substitution and rate heterogeneity.


QUARTET PUZZLING TREE

Support for the internal branches of the unrooted quartet puzzling
tree topology is shown in percent.

This quartet puzzling tree is not completely resolved!


         :---MYG_PHYCA 
         :             
     :100:---GLB5_PETMA
     :   :             
 :100:   :---LGB2_LUPLU
 :   :                 
 :   :   :---HBA_HUMAN 
 :   :100:             
 :       :---HBA_HORSE 
 :                     
 :-----------HBB_HORSE 
 :                     
 :-----------HBB_HUMAN 


Quartet puzzling tree (in CLUSTAL W notation):

(HBB_HUMAN,((MYG_PHYCA,GLB5_PETMA,LGB2_LUPLU)100,(HBA_HUMAN,
HBA_HORSE)100)100,HBB_HORSE);


BIPARTITIONS

The following bipartitions occured at least once in all intermediate
trees that have been generated in the 1000 puzzling steps.
Bipartitions included in the quartet puzzling tree:
(bipartition with sequences in input order : number of times seen)

 ****...  :  1000
 **.....  :  1000
 **..***  :  1000

Congruent bipartitions occurred in 50% or less, not included in 
the consensus tree:
(bipartition with sequences in input order : number of times seen)

 *****..  :  347

Incongruent bipartitions not included in the consensus tree:
(bipartition with sequences in input order : number of times seen)

 ****..*  :  331
 ****.*.  :  322


MAXIMUM LIKELIHOOD BRANCH LENGTHS ON CONSENSUS TREE (NO CLOCK)

Branch lengths are computed using the selected model of
substitution and rate heterogeneity.


                  :-------------5 MYG_PHYCA
          :-------8
          :       :--------6 GLB5_PETMA
          :       :
          :       :------------------7 LGB2_LUPLU
 :-------10
 :        :     :-3 HBA_HUMAN
 :        :-----9
 :              :--4 HBA_HORSE
 :
 :--2 HBB_HORSE
 :
 :-1 HBB_HUMAN
 

         branch  length     S.E.   branch  length     S.E.
HBB_HUMAN     1  0.04599  0.03062       8  0.42025  0.11481
HBB_HORSE     2  0.12231  0.03740       9  0.25241  0.08069
HBA_HUMAN     3  0.03150  0.02213      10  0.54296  0.10035
HBA_HORSE     4  0.08803  0.03011
MYG_PHYCA     5  1.02811  0.16334
GLB5_PETMA    6  0.60661  0.11823     12 iterations until convergence
LGB2_LUPLU    7  1.38558  0.20879     log L: -1703.59


Consensus tree with maximum likelihood branch lengths
(in CLUSTAL W notation):

(HBB_HUMAN:0.04599,((MYG_PHYCA:1.02811,GLB5_PETMA:0.60661,LGB2_LUPLU:1.38558)
100:0.42025,(HBA_HUMAN:0.03150,HBA_HORSE:0.08803)100:0.25241)100:0.54296,
HBB_HORSE:0.12231);


TIME STAMP

