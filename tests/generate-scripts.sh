#! /bin/sh

#
#  generate-scripts.sh
#
#
#  Part of TREE-PUZZLE 5.2 (July 2004)
#
#  (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
#  (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
#                   M. Vingron, and Arndt von Haeseler
#  (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
#
#  All parts of the source except where indicated are distributed under
#  the GNU public licence.  See http://www.opensource.org for details.
#
#  ($Id$)
#

QPSCRIPTS="\
  qp-pure-bin.test\
  qp-pure-nucl.test\
  qp-tn-nucl.test\
  qp-hky-clock-nucl.test\
  qp-hky-rhet-nucl.test\
  qp-hky-rhet-clock-nucl.test\
  qp-pure-prot.test\
  qp-mtrev-prot.test\
  qp-vt-prot.test\
  qp-wag-prot.test\
  qp-clock.test\
  qp-jtt-prot.test\
  qp-jtt-rhet-prot.test\
  qp-jtt-rhet-clock-prot.test\
  "
LMSCRIPT="\
  lm-pure-prot.test\
  "
UTSCRIPTS="\
  ut-pure-prot.test\
  "
UCSCRIPTS="\
  cons-pure-prot.test\
  "

SCRIPTS="$QPSCRIPTS $LMSCRIPT $UTSCRIPTS $UCSCRIPTS"

for i in $SCRIPTS ; do
  echo "  $i"
  cp template-test $i
  chmod a+rx $i
done
