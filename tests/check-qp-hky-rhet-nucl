TREE-PUZZLE 

Input file name: qp-hky-rhet-nucl.nucl
Type of analysis: tree reconstruction
Parameter estimation: approximate (faster)
Parameter estimation uses: neighbor-joining tree (for substitution process and rate variation)

Standard errors (S.E.) are obtained by the curvature method.
The upper and lower bounds of an approximate 95% confidence interval
for parameter or branch length x are x-1.96*S.E. and x+1.96*S.E.


SEQUENCE ALIGNMENT

Input data: 8 sequences with 116 nucleotide sites
Number of constant sites: 66 (= 56.9% of all sites)
Number of site patterns: 54
Number of constant site patterns: 4 (= 7.4% of all site patterns)


SUBSTITUTION PROCESS

Model of substitution: HKY (Hasegawa et al. 1985)
Transition/transversion parameter (estimated from data set): 2.95 (S.E. 0.84)

Rate matrix R (parameters restricted to selected model):

 A-C rate: 1.00000
 A-G rate: 5.85063
 A-T rate: 1.00000
 C-G rate: 1.00000
 C-T rate: 5.85063
 G-T rate: 1.00000

Nucleotide frequencies (estimated from data set):

 pi(A) =  26.6%
 pi(C) =  30.6%
 pi(G) =  12.0%
 pi(T) =  30.7%

Expected transition/transversion ratio: 3.14
Expected pyrimidine transition/purine transition ratio: 2.95


AMBIGUOUS CHARACTERS IN THE SEQUENCE (SEQUENCES IN INPUT ORDER)

               gaps  wildcards        sum   % sequence
 Thylacinus       0          0          0        0.00%   
 Sarcophilu       0          0          0        0.00%   
 Dasyurus         0          1          1        0.86%   
 Echymipera       0          0          0        0.00%   
 Trichosuru       0          0          0        0.00%   
 Phalanger        0          0          0        0.00%   
 Philander        0          0          0        0.00%   
 Bos              0          0          0        0.00%   
 -------------------------------------------------------
 Sum              0          1          1        0.11%   


The table above shows the amount of gaps ('-') and other 'wildcard'
characters ('X', '?', etc.) and their percentage of the 116 columns
in the alignment.
Sequences with more than 50% ambiguous characters are marked with a '!' and 
should be checked, whether they have sufficient overlap to other sequences.
Sequences with 100% ambiguous characters do not hold any phylogenetic
information and had to be discarded from the analysis.



SEQUENCE COMPOSITION (SEQUENCES IN INPUT ORDER)

              5% chi-square test  p-value
 Thylacinus        passed          90.89%  
 Sarcophilu        passed          93.32%  
 Dasyurus          passed          98.01%  
 Echymipera        passed          92.83%  
 Trichosuru        passed          90.41%  
 Phalanger         passed          86.09%  
 Philander         passed          66.19%  
 Bos               passed          60.02%  

The chi-square tests compares the nucleotide composition of each sequence
to the frequency distribution assumed in the maximum likelihood model.


IDENTICAL SEQUENCES

The sequences in each of the following groups are all identical. To speed
up computation please remove all but one of each group from the data set.

 All sequences are unique.


MAXIMUM LIKELIHOOD DISTANCES

Maximum likelihood distances are computed using the selected model of
substitution and rate heterogeneity.

  8
Thylacinus  0.00000  0.52940  0.37900  0.52236  0.58922  0.56150  0.43932
            0.55291
Sarcophilu  0.52940  0.00000  0.17837  0.62532  0.69892  0.93602  0.62327
            0.78596
Dasyurus    0.37900  0.17837  0.00000  0.39032  0.53431  0.60369  0.31919
            0.56220
Echymipera  0.52236  0.62532  0.39032  0.00000  0.70229  0.94283  0.42016
            0.66063
Trichosuru  0.58922  0.69892  0.53431  0.70229  0.00000  0.39416  0.70017
            0.62697
Phalanger   0.56150  0.93602  0.60369  0.94283  0.39416  0.00000  0.78658
            0.98921
Philander   0.43932  0.62327  0.31919  0.42016  0.70017  0.78658  0.00000
            0.49264
Bos         0.55291  0.78596  0.56220  0.66063  0.62697  0.98921  0.49264
            0.00000

Average distance (over all possible pairs of sequences):  0.59096
                  minimum  : 0.17837,  maximum  : 0.98921
                  variance : 0.03581,  std.dev. : 0.18924


RATE HETEROGENEITY

Model of rate heterogeneity: Gamma distributed rates
Gamma distribution parameter alpha (estimated from data set): 0.25 (S.E. 0.05)
Number of Gamma rate categories: 8

Rates and their respective probabilities used in the likelihood function:

 Category  Relative rate  Probability
  1         0.0000         0.1250
  2         0.0031         0.1250
  3         0.0262         0.1250
  4         0.1071         0.1250
  5         0.3140         0.1250
  6         0.7768         0.1250
  7         1.8180         0.1250
  8         4.9547         0.1250

Categories 1-8 approximate a continous Gamma-distribution with expectation 1
and variance 3.94.

Combination of categories that contributes the most to the likelihood
(computation done without clock assumption assuming quartet-puzzling tree):

 7 1 1 7 1 1 8 1 1 8   7 1 7 6 1 6 1 1 7 8   1 8 1 1 1 7 1 1 7 5
 8 6 1 8 1 1 1 1 1 5   6 1 8 1 1 6 1 1 7 7   1 1 1 1 7 7 1 1 1 1
 7 1 1 6 1 1 7 1 1 5   1 1 8 6 1 8 1 1 6 1   1 6 8 7 7 1 1 8 1 1
 8 1 1 7 1 1 8 1 1 8   5 1 8 5 1 7 1 1 7 1   1 6 1 5 7 1


QUARTET STATISTICS (SEQUENCES IN INPUT ORDER)


 name       | resolved        | partly resolved | unresolved      | sum
 --------------------------------------------------------------------------
 Thylacinus       15 [ 42.86%]       0 [  0.00%]      20 [ 57.14%]      35
 Sarcophilu       16 [ 45.71%]       1 [  2.86%]      18 [ 51.43%]      35
 Dasyurus         19 [ 54.29%]       3 [  8.57%]      13 [ 37.14%]      35
 Echymipera       12 [ 34.29%]       0 [  0.00%]      23 [ 65.71%]      35
 Trichosuru       16 [ 45.71%]       2 [  5.71%]      17 [ 48.57%]      35
 Phalanger        20 [ 57.14%]       2 [  5.71%]      13 [ 37.14%]      35
 Philander        13 [ 37.14%]       1 [  2.86%]      21 [ 60.00%]      35
 Bos               9 [ 25.71%]       3 [  8.57%]      23 [ 65.71%]      35
 --------------------------------------------------------------------------
  #quartets :     30 [ 42.86%]       3 [  4.29%]      37 [ 52.86%]      70

The table shows the occurrences of fully resolved, partially, and
completely unresolved quartets for each sequence and their percentage
relative to the number of times the sequence occurs in the list of 
quartets (i.e. 35 quartets out of 70 in total).
In fully resolved quartet one single topology is supported, while for
partially resolved quartets two and for completely unresolved quartets
none of the topologies (AB||CD, AC||BD, AD||BC) are favoured.
Note: Because 4 sequences are involved in one quartet numbers add up
to a four-fold of the existing quartets.

Hint: The overall numbers in the last row give information about the
phylogenetic content of the dataset. The higher the percentage of partially
and unresolved quartets, the lower the content of phylogenetic information.
This can be visualized in more detail by likelihood mapping analysis.



TREE SEARCH

Quartet puzzling is used to choose from the possible tree topologies
and to simultaneously infer support values for internal branches.

Number of puzzling steps: 1000
Analysed quartets: 70
Fully resolved quartets:  30 (= 42.9%)
Partly resolved quartets: 3 (= 4.3%)
Unresolved quartets:      37 (= 52.9%)

Quartet trees are based on approximate maximum likelihood values
using the selected model of substitution and rate heterogeneity.


QUARTET PUZZLING TREE

Support for the internal branches of the unrooted quartet puzzling
tree topology is shown in percent.

This quartet puzzling tree is not completely resolved!


     :---Sarcophilu
 :-90:             
 :   :---Dasyurus  
 :                 
 :   :---Trichosuru
 :-87:             
 :   :---Phalanger 
 :                 
 :-------Echymipera
 :                 
 :-------Philander 
 :                 
 :-------Bos       
 :                 
 :-------Thylacinus


Quartet puzzling tree (in CLUSTAL W notation):

(Thylacinus,(Sarcophilu,Dasyurus)90,(Trichosuru,Phalanger)87,
Echymipera,Philander,Bos);


BIPARTITIONS

The following bipartitions occured at least once in all intermediate
trees that have been generated in the 1000 puzzling steps.
Bipartitions included in the quartet puzzling tree:
(bipartition with sequences in input order : number of times seen)

 *..*****  :  904
 ****..**  :  872

Congruent bipartitions occurred in 50% or less, not included in 
the consensus tree:
(bipartition with sequences in input order : number of times seen)

 *...**..  :  377
 ******..  :  357

Incongruent bipartitions not included in the consensus tree:
(bipartition with sequences in input order : number of times seen)

 ***.**.*  :  220
 ***.**..  :  218
 *...****  :  205
 ***.***.  :  145
 *......*  :  135
 *..*..**  :  129
 *...**.*  :  124
 *.....**  :  106
 *..*....  :  102
 *..***.*  :  92
 *..***..  :  91
 ***.....  :  86
 *.....*.  :  71
 ***...**  :  70
 *..****.  :  67
 ****..*.  :  67
 *....*..  :  64
 *...***.  :  54
 ****....  :  52
 *..*...*  :  47

(49 other less frequent bipartitions not shown)


MAXIMUM LIKELIHOOD BRANCH LENGTHS ON CONSENSUS TREE (NO CLOCK)

Branch lengths are computed using the selected model of
substitution and rate heterogeneity.


         :------2 Sarcophilu
 :-------9
 :       :-3 Dasyurus
 :
 :        :-----5 Trichosuru
 :-------10
 :        :--------6 Phalanger
 :
 :--------4 Echymipera
 :
 :-------7 Philander
 :
 :-----------8 Bos
 :
 :-------1 Thylacinus
 

         branch  length     S.E.   branch  length     S.E.
Thylacinus    1  0.23363  0.10248       9  0.19927  0.09576
Sarcophilu    2  0.18905  0.06712      10  0.22291  0.13093
Dasyurus      3  0.00001  0.00200
Echymipera    4  0.27841  0.11691
Trichosuru    5  0.17822  0.10160
Phalanger     6  0.27519  0.12613
Philander     7  0.22533  0.10269     10 iterations until convergence
Bos           8  0.38507  0.14566     log L: -570.33

WARNING --- at least one branch length is close to an internal boundary!


Consensus tree with maximum likelihood branch lengths
(in CLUSTAL W notation):

(Thylacinus:0.23363,(Sarcophilu:0.18905,Dasyurus:0.00001)90:0.19927,
(Trichosuru:0.17822,Phalanger:0.27519)87:0.22291,Echymipera:0.27841,
Philander:0.22533,Bos:0.38507);


TIME STAMP

