TREE-PUZZLE 

Input file name: qp-hky-clock-nucl.nucl
Type of analysis: tree reconstruction
Parameter estimation: approximate (faster)
Parameter estimation uses: neighbor-joining tree (for substitution process and rate variation)

Standard errors (S.E.) are obtained by the curvature method.
The upper and lower bounds of an approximate 95% confidence interval
for parameter or branch length x are x-1.96*S.E. and x+1.96*S.E.


SEQUENCE ALIGNMENT

Input data: 8 sequences with 116 nucleotide sites
Number of constant sites: 66 (= 56.9% of all sites)
Number of site patterns: 54
Number of constant site patterns: 4 (= 7.4% of all site patterns)


SUBSTITUTION PROCESS

Model of substitution: HKY (Hasegawa et al. 1985)
Transition/transversion parameter (estimated from data set): 1.51 (S.E. 0.28)

Rate matrix R (parameters restricted to selected model):

 A-C rate: 1.00000
 A-G rate: 2.98614
 A-T rate: 1.00000
 C-G rate: 1.00000
 C-T rate: 2.98614
 G-T rate: 1.00000

Nucleotide frequencies (estimated from data set):

 pi(A) =  26.6%
 pi(C) =  30.6%
 pi(G) =  12.0%
 pi(T) =  30.7%

Expected transition/transversion ratio: 1.60
Expected pyrimidine transition/purine transition ratio: 2.95


AMBIGUOUS CHARACTERS IN THE SEQUENCE (SEQUENCES IN INPUT ORDER)

               gaps  wildcards        sum   % sequence
 Thylacinus       0          0          0        0.00%   
 Sarcophilu       0          0          0        0.00%   
 Dasyurus         0          1          1        0.86%   
 Echymipera       0          0          0        0.00%   
 Trichosuru       0          0          0        0.00%   
 Phalanger        0          0          0        0.00%   
 Philander        0          0          0        0.00%   
 Bos              0          0          0        0.00%   
 -------------------------------------------------------
 Sum              0          1          1        0.11%   


The table above shows the amount of gaps ('-') and other 'wildcard'
characters ('X', '?', etc.) and their percentage of the 116 columns
in the alignment.
Sequences with more than 50% ambiguous characters are marked with a '!' and 
should be checked, whether they have sufficient overlap to other sequences.
Sequences with 100% ambiguous characters do not hold any phylogenetic
information and had to be discarded from the analysis.



SEQUENCE COMPOSITION (SEQUENCES IN INPUT ORDER)

              5% chi-square test  p-value
 Thylacinus        passed          90.89%  
 Sarcophilu        passed          93.32%  
 Dasyurus          passed          98.01%  
 Echymipera        passed          92.83%  
 Trichosuru        passed          90.41%  
 Phalanger         passed          86.09%  
 Philander         passed          66.19%  
 Bos               passed          60.02%  

The chi-square tests compares the nucleotide composition of each sequence
to the frequency distribution assumed in the maximum likelihood model.


IDENTICAL SEQUENCES

The sequences in each of the following groups are all identical. To speed
up computation please remove all but one of each group from the data set.

 All sequences are unique.


MAXIMUM LIKELIHOOD DISTANCES

Maximum likelihood distances are computed using the selected model of
substitution and rate heterogeneity.

  8
Thylacinus  0.00000  0.25562  0.22032  0.24509  0.26007  0.24938  0.22246
            0.23734
Sarcophilu  0.25562  0.00000  0.13169  0.26969  0.28414  0.32680  0.26008
            0.29786
Dasyurus    0.22032  0.13169  0.00000  0.20032  0.24821  0.25234  0.17801
            0.23883
Echymipera  0.24509  0.26969  0.20032  0.00000  0.29531  0.33871  0.23041
            0.27215
Trichosuru  0.26007  0.28414  0.24821  0.29531  0.00000  0.21136  0.28562
            0.27148
Phalanger   0.24938  0.32680  0.25234  0.33871  0.21136  0.00000  0.29036
            0.33042
Philander   0.22246  0.26008  0.17801  0.23041  0.28562  0.29036  0.00000
            0.22452
Bos         0.23734  0.29786  0.23883  0.27215  0.27148  0.33042  0.22452
            0.00000

Average distance (over all possible pairs of sequences):  0.25459
                  minimum  : 0.13169,  maximum  : 0.33871
                  variance : 0.00207,  std.dev. : 0.04544


RATE HETEROGENEITY

Model of rate heterogeneity: uniform rate


QUARTET STATISTICS (SEQUENCES IN INPUT ORDER)


 name       | resolved        | partly resolved | unresolved      | sum
 --------------------------------------------------------------------------
 Thylacinus       22 [ 62.86%]       4 [ 11.43%]       9 [ 25.71%]      35
 Sarcophilu       27 [ 77.14%]       1 [  2.86%]       7 [ 20.00%]      35
 Dasyurus         27 [ 77.14%]       4 [ 11.43%]       4 [ 11.43%]      35
 Echymipera       20 [ 57.14%]       4 [ 11.43%]      11 [ 31.43%]      35
 Trichosuru       27 [ 77.14%]       2 [  5.71%]       6 [ 17.14%]      35
 Phalanger        28 [ 80.00%]       3 [  8.57%]       4 [ 11.43%]      35
 Philander        25 [ 71.43%]       6 [ 17.14%]       4 [ 11.43%]      35
 Bos              20 [ 57.14%]       4 [ 11.43%]      11 [ 31.43%]      35
 --------------------------------------------------------------------------
  #quartets :     49 [ 70.00%]       7 [ 10.00%]      14 [ 20.00%]      70

The table shows the occurrences of fully resolved, partially, and
completely unresolved quartets for each sequence and their percentage
relative to the number of times the sequence occurs in the list of 
quartets (i.e. 35 quartets out of 70 in total).
In fully resolved quartet one single topology is supported, while for
partially resolved quartets two and for completely unresolved quartets
none of the topologies (AB||CD, AC||BD, AD||BC) are favoured.
Note: Because 4 sequences are involved in one quartet numbers add up
to a four-fold of the existing quartets.

Hint: The overall numbers in the last row give information about the
phylogenetic content of the dataset. The higher the percentage of partially
and unresolved quartets, the lower the content of phylogenetic information.
This can be visualized in more detail by likelihood mapping analysis.



TREE SEARCH

Quartet puzzling is used to choose from the possible tree topologies
and to simultaneously infer support values for internal branches.

Number of puzzling steps: 1000
Analysed quartets: 70
Fully resolved quartets:  49 (= 70.0%)
Partly resolved quartets: 7 (= 10.0%)
Unresolved quartets:      14 (= 20.0%)

Quartet trees are based on approximate maximum likelihood values
using the selected model of substitution and rate heterogeneity.


QUARTET PUZZLING TREE

Support for the internal branches of the unrooted quartet puzzling
tree topology is shown in percent.

This quartet puzzling tree is not completely resolved!


         :---Trichosuru
 :-----97:             
 :       :---Phalanger 
 :                     
 :       :---Sarcophilu
 :   :-98:             
 :   :   :---Dasyurus  
 :   :                 
 :-71:   :---Philander 
 :   :-65:             
 :   :   :---Bos       
 :   :                 
 :   :-------Echymipera
 :                     
 :-----------Thylacinus


Quartet puzzling tree (in CLUSTAL W notation):

(Thylacinus,(Trichosuru,Phalanger)97,((Sarcophilu,Dasyurus)98,
(Philander,Bos)65,Echymipera)71);


BIPARTITIONS

The following bipartitions occured at least once in all intermediate
trees that have been generated in the 1000 puzzling steps.
Bipartitions included in the quartet puzzling tree:
(bipartition with sequences in input order : number of times seen)

 *..*****  :  981
 ****..**  :  971
 *...**..  :  713
 ******..  :  649

Congruent bipartitions occurred in 50% or less, not included in 
the consensus tree:
(bipartition with sequences in input order : number of times seen)

 *...****  :  426

Incongruent bipartitions not included in the consensus tree:
(bipartition with sequences in input order : number of times seen)

 ***.**..  :  347
 ***.**.*  :  150
 *...**.*  :  131
 *..***..  :  87
 *..*..**  :  81
 *.....**  :  72
 ****..*.  :  55
 ***.***.  :  37
 ****....  :  36
 *..***.*  :  36
 ***.....  :  31
 *.....*.  :  28
 *...***.  :  27
 *......*  :  27
 *....*..  :  26
 *..*....  :  21
 **..****  :  16
 ***...**  :  14
 *..*..*.  :  13
 *..****.  :  12

(6 other less frequent bipartitions not shown)


MAXIMUM LIKELIHOOD BRANCH LENGTHS ON CONSENSUS TREE (NO CLOCK)

Branch lengths are computed using the selected model of
substitution and rate heterogeneity.


        :---------5 Trichosuru
 :------9
 :      :----------6 Phalanger
 :
 :               :-----------2 Sarcophilu
 :      :-------10
 :      :        :-3 Dasyurus
 :-----12
 :      :     :--------7 Philander
 :      :----11
 :      :     :-----------8 Bos
 :      :
 :      :------------4 Echymipera
 :
 :---------1 Thylacinus
 

         branch  length     S.E.   branch  length     S.E.
Thylacinus    1  0.11024  0.03812       9  0.05953  0.03382
Sarcophilu    2  0.13180  0.03734      10  0.08585  0.03452
Dasyurus      3  0.00572  0.01273      11  0.04220  0.02513
Echymipera    4  0.13997  0.04108      12  0.05457  0.02998
Trichosuru    5  0.10654  0.03770
Phalanger     6  0.12258  0.04064
Philander     7  0.08995  0.03428     8 iterations until convergence
Bos           8  0.13479  0.04095     log L: -603.37


Consensus tree with maximum likelihood branch lengths
(in CLUSTAL W notation):

(Thylacinus:0.11024,(Trichosuru:0.10654,Phalanger:0.12258)97:0.05953,
((Sarcophilu:0.13180,Dasyurus:0.00572)98:0.08585,(Philander:0.08995,
Bos:0.13479)65:0.04220,Echymipera:0.13997)71:0.05457);


MAXIMUM LIKELIHOOD BRANCH LENGTHS OF CONSENSUS TREE (WITH CLOCK)

Branch lengths are computed using the selected model of
substitution and rate heterogeneity.

Root located at branch: 1 "Thylacinus"  (automatic search)

If the automatic search misplaces the root please rerun the analysis
(rename "outtree" to "intree") and select location of root manually!


       :---------5 Trichosuru
 :-----9
 :     :---------6 Phalanger
 :
 :           :-----2 Sarcophilu
 :   :------10
 :   :       :-----3 Dasyurus
 :--12
 :   :    :--------7 Philander
 :   :---11
 :   :    :--------8 Bos
 :   :
 :   :-----------4 Echymipera
 :
 :------------1 Thylacinus
 

Tree drawn as unrooted tree for better comparison with non-clock tree!

         branch  length     nc/c   branch  length     nc/c (= non-clock/clock)
Thylacinus    1  0.14706    0.750       9  0.04504    1.322
Sarcophilu    2  0.06189    2.130      10  0.06663    1.288
Dasyurus      3  0.06189    0.092      11  0.03304    1.277
Echymipera    4  0.12852    1.089      12  0.01851    2.948
Trichosuru    5  0.10199    1.045
Phalanger     6  0.10199    1.202
Philander     7  0.09549    0.942     6 iterations until convergence
Bos           8  0.09549    1.412     log L: -610.75

 height    S.E.    of node common to branches
0.10199  0.01922    9  5  6  
0.06189  0.01495    10  2  3  
0.09549  0.01498    11  7  8  
0.12852  0.01108    12  10  11  4  
0.14704  0.01235    1  9  12  
0.14705  0.02366   of root at branch 1


Rooted consensus tree with clocklike maximum likelihood branch lengths
(in CLUSTAL W notation):

(Thylacinus:0.14705,((Trichosuru:0.10199,Phalanger:0.10199)97:0.04504
,((Sarcophilu:0.06189,Dasyurus:0.06189)98:0.06663,(Philander:0.09549,
Bos:0.09549)65:0.03304,Echymipera:0.12852)71:0.01851):0.00001);


MOLECULAR CLOCK LIKELIHOOD RATIO TEST

log L without clock: -603.37 (independent branch parameters: 12)
log L with clock:    -610.75 (independent branch parameters: 6)

Likelihood ratio test statistic delta: 14.77
Degress of freedom of chi-square distribution: 6
Critical significance level: 2.22%

The simpler (clocklike) tree is rejected on a significance level
of 5%. The log-likelihood of the more complex (no clock) tree is
significantly increased.

Please take care that the correct root is used!


TIME STAMP

