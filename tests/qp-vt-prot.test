#! /bin/sh

#
#  template-test
#
#
#  Part of TREE-PUZZLE 5.2 (July 2004)
#
#  (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
#  (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
#                   M. Vingron, and Arndt von Haeseler
#  (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
#
#  All parts of the source except where indicated are distributed under
#  the GNU public licence.  See http://www.opensource.org for details.
#
#  ($Id$)
#

fname=`echo $0 | sed 's/.*\///'`
GENERATE=0
TESTTYPE="Testing: "

if test 0 -ne $# 
then
   case "$1" in
       -g*)
           GENERATE=1
           TESTTYPE="Generating: "
           ;;
       *)
           echo "Usage: $fname [-g] "
           echo
           echo "  Test a TREE-PUZZLE scenario."
           echo "  With -g no test is performed, but the "
           echo "  template for a future test is created."
           echo
           exit 1
           ;;
   esac
fi

##########################################################################

TEST_PREF=`echo $fname | sed s/\.test//`
INUTREE=${TEST_PREF}.trees
INCTREE=${TEST_PREF}.ctrees
PARAMS=${TEST_PREF}.param
# default
DATA=nucl
INFILE=${TEST_PREF}.${DATA}
INFILESOURCE=../data/marswolf.n
INUTREESOURCE=../data/marswolf.trees
INCTREESOURCE=../data/marswolf.ctrees

#
# data type
#
case "$TEST_PREF" in 
   *-nucl)
          TESTTYPE="${TESTTYPE}nucleotide data"
          DATA=nucl
          INFILE=${TEST_PREF}.${DATA}
          INFILESOURCE=../data/marswolf.n
          INUTREESOURCE=../data/marswolf.trees
          INCTREESOURCE=../data/marswolf.ctrees
          echo d > $PARAMS
          case "$TEST_PREF" in 
              *-hky*)
                       TESTTYPE="${TESTTYPE}, HKY"
                       ;;
              *-tn*)
                       TESTTYPE="${TESTTYPE}, TN"
                       echo m >> $PARAMS
                       ;;
              *-gtr*)
                       TESTTYPE="${TESTTYPE}, TN"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *-sh*)
                       TESTTYPE="${TESTTYPE}, SH"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *)
                       TESTTYPE="${TESTTYPE}, default model"
          esac
          ;;
   *-prot)
          TESTTYPE="${TESTTYPE}protein data"
          DATA=prot
          INFILE=${TEST_PREF}.${DATA}
          INFILESOURCE=../data/globin.a
          INUTREESOURCE=../data/globin.trees
          INCTREESOURCE=../data/globin.ctrees
          echo d > $PARAMS
          echo d >> $PARAMS
          case "$TEST_PREF" in 
              *-dayhoff*)
                       TESTTYPE="${TESTTYPE}, Dayhoff"
                       echo m >> $PARAMS
                       ;;
              *-jtt*)
                       TESTTYPE="${TESTTYPE}, JTT"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *-mtrev*)
                       TESTTYPE="${TESTTYPE}, mtREV"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *-blosum*)
                       TESTTYPE="${TESTTYPE}, BLOSUM"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *-vt*)
                       TESTTYPE="${TESTTYPE}, VT"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *-wag*)
                       TESTTYPE="${TESTTYPE}, WAG"
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       echo m >> $PARAMS
                       ;;
              *)
                       TESTTYPE="${TESTTYPE}, default model"
          esac
          ;;
   *-bin)
          TESTTYPE="${TESTTYPE}binary data"
          DATA=bin
          INFILE=${TEST_PREF}.${DATA}
          INFILESOURCE=../data/primates.b
          INUTREESOURCE=../data/primates.trees
          INCTREESOURCE=../data/primates.ctrees
          echo d > $PARAMS
          echo d >> $PARAMS
          echo d >> $PARAMS
          ;;
   *)
          TESTTYPE="${TESTTYPE}default data, default model"
          ;;
esac

case "$TEST_PREF" in 
    *-clock*)
             TESTTYPE="${TESTTYPE}, clock"
             echo z >> $PARAMS
             ;;
esac
case "$TEST_PREF" in 
    *-rhet4*)
             TESTTYPE="${TESTTYPE}, rate heterogeneity (4 rates)"
             echo w >> $PARAMS
             echo c >> $PARAMS
             echo 4 >> $PARAMS
             ;;
    *-rhet*)
             TESTTYPE="${TESTTYPE}, rate heterogeneity"
             echo w >> $PARAMS
             ;;
esac

CHECK_PREF=check-$INFILE

case "$TEST_PREF" in 
    qp-*)
             TESTTYPE="${TESTTYPE}, quartet puzzling"
             cp $INFILESOURCE $INFILE
             RESULT_PREF=$INFILE
             echo y >> $PARAMS
             echo; echo "$TESTTYPE ($TEST_PREF)"
             ../src/puzzle$EXEEXT -randseed1001 ${INFILE} < $PARAMS > ${INFILE}.out 2>&1
             ;;
    lm-*)
             TESTTYPE="${TESTTYPE}, likelihood mapping"
             cp $INFILESOURCE $INFILE
             RESULT_PREF=$INFILE
             echo b >> $PARAMS
             echo y >> $PARAMS
             echo; echo "$TESTTYPE ($TEST_PREF)"
             ../src/puzzle$EXEEXT -randseed1001 ${INFILE} < $PARAMS > ${INFILE}.out 2>&1
             ;;
    ut-*)
             TESTTYPE="${TESTTYPE}, user tree evaluation"
             cp $INFILESOURCE $INFILE
             cp $INUTREESOURCE $INUTREE
             RESULT_PREF=$INUTREE
             echo y >> $PARAMS
             echo; echo "$TESTTYPE ($TEST_PREF)"
             ../src/puzzle$EXEEXT -randseed1001 ${INFILE} ${INUTREE} < $PARAMS > ${INFILE}.out 2>&1
             ;;
    cons-*)
             TESTTYPE="${TESTTYPE}, consensus construction"
             cp $INFILESOURCE $INFILE
             cp $INCTREESOURCE $INCTREE
             RESULT_PREF=$INCTREE
             echo k >> $PARAMS
             echo y >> $PARAMS
             echo; echo "$TESTTYPE ($TEST_PREF)"
             ../src/puzzle$EXEEXT -randseed1001 ${INFILE} ${INCTREE} < $PARAMS > ${INFILE}.out 2>&1
             ;;
    *)
             echo; echo "$TESTTYPE ($TEST_PREF)"
             echo UNKNOWN analysis type: test ignored
             exit 77
             ;;
esac

if test $GENERATE == 1; then
   cat ${RESULT_PREF}.puzzle \
      | grep -v "^Date and time" \
      | grep -v "^Runtime (" \
      | sed "s/$VERSION//g" \
      > check-${TEST_PREF}
   exit 0
else
   cat ${RESULT_PREF}.puzzle \
      | grep -v "^Date and time" \
      | grep -v "^Runtime (" \
      | sed "s/$VERSION//g" \
      > ${RESULT_PREF}.clean
   diff ${RESULT_PREF}.clean check-${TEST_PREF}
   result=$?
   exit $result
fi
