/*
 * gamma.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

#ifndef _GAMMA_
#define _GAMMA_

double densityGamma (double, double);
double cdfGamma (double, double);
double icdfGamma (double, double);
double momentGamma (int, double);

double LnGamma (double);
double IncompleteGammaQ (double, double);

double chi2prob (int, double);
double chi2test (double *, int *, int , int *);


#endif /* _GAMMA_ */
