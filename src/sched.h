/*
 * sched.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */


#ifndef SCHED_H
#define SCHED_H
#ifndef SCHEDTEST
#   include "util.h" 
#else
    typedef unsigned long int uli; 
#endif


typedef struct sched_t{
   uli    truetasks;    /* number of tasks to be scheduled, as given by user */
   int    numprocs;     /* number of processors (p=PP_numprocs-1, master-worker scheme */
   uli    minchunk;     /* pre-packing: task-size must be dividable by <minchunk> */

   uli    rest;         /* part of tasks not fitting into batches with minchunk-dividable size */
                        /*    truetasks mod minchunk */
   uli    alltasks;     /* number of tasks to be scheduled in minchunk-size batches */
                        /*    truetasks - rest */
   uli    numtasks;     /* tasks not yet scheduled (init: numtasks = alltasks) */

   uli    overhead;     /* SC,FSC: remainder from dividing alltasks into numprocs batches */
                        /*   alltasks mod numprocs */
   uli    delta;        /* SC,FSC: initial batch size */
                        /*   (alltasks - overhead) / numprocs = alltasks % numprocs */

   double fconst;       /* TSS: start (first) batch size (f) */
   double lconst;       /* TSS: last batch size (l) */
   double kconst;       /* TSS: actual batch size (k) */
   double ddelta;       /* TSS: amount of decrease */
   int    nconst;       /* TSS: needed to compute ddelta (N) */

   int    inited;       /* initialized flag for parameter initialization by scheduler */
} schedtype;

void num2quart(uli qnum, int *a, int *b, int *c, int *d);
uli numquarts(int maxspc);
uli quart2num (int a, int b, int c, int d);

void printsched(schedtype sch);
void initsched(schedtype *sch, uli tasks, int procs, uli minchunk);
uli sc(schedtype *sch);
uli gss(schedtype *sch);
uli sgss(schedtype *sch);
uli tss(schedtype *sch);

#endif /* SCHED_H */
