/*
 *   pstep-orig.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

#ifndef PSTEP_ORIG_H
#define PSTEP_ORIG_H

#include<stdio.h>
#include<stdlib.h>
#include"puzzle.h"
#include"util.h"
/* #include"newpstep.h" */
#include<stdlib.h>

#ifndef ONEEDGE_DEFINED
#define ONEEDGE_DEFINED
	/* tree structure */
	typedef struct oneedge {
        	/* pointer to other three edges */
        	struct oneedge *up;
        	struct oneedge *downleft;
        	struct oneedge *downright;
        	int numedge;    /* number of edge */
        	uli edgeinfo;   /* value of this edge */
        	int *edgemap;   /* pointer to the local edgemap */
	} ONEEDGE;
#endif /* ! ONEEDGE_DEFINED */

EXTERN int *edgeofleaf;      /* vector with edge number of all leaves */
EXTERN int minedge;          /* edge with minimum edgeinfo */
EXTERN int nextedge;         /* number of edges in the current tree */
EXTERN int nextleaf;         /* next leaf to add to tree */

EXTERN ONEEDGE *edge;        /* vector with all the edges of the tree */
uli *splitcomp;       /* bipartition storage */
uli *splitfreqs;      /* frequencies of all different splits of all trees */
uli *splitpatterns;   /* all different splits of all trees */

#if 0
EXTERN int Maxbrnch;         /* 2*Maxspc - 3 */
EXTERN int Maxspc;           /* Maxspc */
#endif

ivector trueID;
int column;
uli mininfo;          /* value of edgeinfo on minedge */


/*****************************************************************************/
/* functions for representing and building puzzling step trees               */
/*****************************************************************************/

/* initialize tree with the following starting configuration
 *
 *                2
 *         0  +------- C(=2)
 * A(=0) -----+
 *            +------- B(=1)
 *                1
 */


void inittree(void);
void addnextleaf(int);
void freetree(void);
void writeOTU(FILE *, int);
void writetree(FILE *);
void resetedgeinfo(void);
void incrementedgeinfo(int, int);
void minimumedgeinfo(void);

#endif /* PSTEP_ORIG_H */

