/*
 * subsetmatr.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

#ifndef SUBSETMATR_H
#define SUBSETMATR_H

#include <stdio.h>
#include <stdlib.h>
#include "util.h"

void printssmatr(cmatrix subsetmatr, int subsets, int seqs);
void printsubsets(imatrix sssets, ivector sssizes, int subsets, int seqs);
void readssmatr(FILE *fp, cmatrix *ssmatr, imatrix *sslists, ivector *sssizes, int *subsets, int *seqs);
void writessmatr(FILE *fp, cmatrix subsetmatr, int subsets, int seqs);
void chooserfromsubsets(cmatrix ssmatr, imatrix sssets, ivector sssizes, int numsets, int numspecs, ivector trueid);

#endif /* SUBSETMATR_H */
