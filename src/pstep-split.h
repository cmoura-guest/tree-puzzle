/*
 *   pstep-split.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

/* split based O(n^4) algorithm */

#ifndef PSTEP_SPLIT_H
#define PSTEP_SPLIT_H

#include<stdio.h>
#include<stdlib.h>
#include"puzzle.h"
#include"util.h"
/* #include"newpstep.h" */

#ifndef ONEEDGE
#	define ONEEDGE ONEEDGE_SPLIT
#endif /* ! ONEEDGE_DEFINED */

#define NOWHERE     0
#define UP          1
#define UPDOWNLEFT  2
#define UPDOWNRIGHT 3
#define DOWNLEFT    4
#define DOWNRIGHT   5
#define STARTING    6


/* tree structure */
typedef struct oneedge_split {
       	/* pointer to other three edges */
       	struct oneedge_split *up;
       	struct oneedge_split *downleft;
       	struct oneedge_split *downright;
       	int numedge;         /* number of edge (index) */
	uli penalty;         /* penalty of this edge */
       	int *split;          /* cluster array (up: 0..upsize-1; down: maxspc-1..maxspc-downsize */
       	int  upsize;         /* size of root-ward cluster */
       	int  downsize;       /* size of non-root-ward cluster */
#if 0
       	uli edgeinfo;        /* value of this edge (penalty) */
       	int *edgemap;        /* _orig algorithm: pointer to the local edgemap array */
       	int *upcluster;      /* cluster array of root-ward taxa */
       	int *downcluster;    /* cluster array of non-root-ward taxa */
#endif
} ONEEDGE_SPLIT;


/*****************************************************************************/
/* internal functions for representing and building puzzling step trees      */
/*****************************************************************************/

/* initialize tree with the following starting configuration
 *
 *                2
 *         0  +------- C(=2)
 * A(=0) -----+
 *            +------- B(=1)
 *                1
 *
 *
 *               A(=0)
 *               [up       =NULL]
 *               [downleft =1]
 *               [downright=2]
 *               [upcluster=0]
 *               [downcluster=1,2]
 *               [upsize=1]
 *               [downsize=2]
 *
 *                    o
 *                    |
 *                    |
 *      +-------------+--------------+
 *      |                            |
 *      |                            |
 *      o                            o
 *
 *   C(=1)                        B(=2)
 *   [up       =0]                [up       =0]
 *   [downleft =NULL]             [downleft =NULL]
 *   [downright=NULL]             [downright=NULL]
 *   [upcluster=0,2]              [upcluster=0]
 *   [downcluster=1]              [downcluster=2]
 *   [upsize=2]                   [upsize=2]
 *   [downsize=1]                 [downsize=1]
 *
 *   nextedge = 3
 *   nextleaf = 3
 *   and set according edge maps
 *
 */
void inittree_split(ONEEDGE_SPLIT **edge,	/* out: new array of edges          */
                   int     **edgeofleaf,/* out: array of external edge ptrs */
                   int       Maxspc, 	/* in:  Number of species (n)       */
                   int       Maxbrnch, 	/* in:  Number of branches (2n-3)   */
                   int      *nextedge, 	/* out: next free edge index (=3)   */
                   int      *nextleaf);	/* out: next free leaf index (=3)   */

/* add next leaf on the specified edge */
void addnextleaf_split(int      dockedge,	/* insert here         */
                      ONEEDGE_SPLIT *edge, 	/* edge array          */
                      int     *edgeofleaf, 	/* ext. edge idx array */
                      int      Maxspc, 		/* No. of species      */
                      int      Maxbrnch, 	/* No. of branches     */
                      int     *nextedge, 	/* next free edge idx  */
                      int     *nextleaf);	/* next free leaf idx  */

/* free memory (to be called after inittree) */
void freetree_split(ONEEDGE_SPLIT *edge,		/* edge array          */
                   int     *edgeofleaf, 	/* ext. edge idx array */
                   int      Maxspc);		/* No. of species      */

/* writes OTU sitting on edge ed */
void writeOTU_split(FILE    *outfp, 		/* output file          */
                   int      ed, 		/* edge to subtree      */
                   ONEEDGE_SPLIT *edge, 		/* edge array           */
                   int     *edgeofleaf, 	/* ext. edge idx array  */
                   int      nextedge, 		/* next free edge idx   */
                   int      nextleaf,		/* next free leaf idx   */
                   int     *column,		/* current screen depth */
                   int     *trueID);		/* species permutation  */

/* write tree */
void writetree_split(FILE   *outfp,		/* output file          */
                   ONEEDGE_SPLIT *edge, 		/* edge array           */
                   int     *edgeofleaf, 	/* ext. edge idx array  */
                   int      nextedge, 		/* next free edge idx   */
                   int      nextleaf,		/* next free leaf idx   */
                   int     *trueID);		/* species permutation  */

/* clear all edgeinfos */
void resetedgeinfo_split(ONEEDGE_SPLIT *edge, 	/* edge array           */
                        int      nextedge);	/* next free edge idx   */

/* increment all edgeinfo between leaf A and B */
void incrementedgeinfo_split(int      A, 	/* start leaf of penalty path */
                            int      B,		/* start leaf of penalty path */
                            ONEEDGE_SPLIT *edge,	  /* edge array           */
                            int     *edgeofleaf); /* ext. edge idx array  */

/* checks which edge has the lowest edgeinfo
   if there are several edges with the same lowest edgeinfo,
   one of them will be selected randomly */
void minimumedgeinfo_split(ONEEDGE_SPLIT *edge, 	/* edge array           */
                          int     *edgeofleaf, 	/* ext. edge idx array  */
                          int      nextedge,	/* next free edge idx   */ 
                          int      nextleaf,	/* next free leaf idx   */
                          int     *minedge, 	/* minimum edge set     */
                          uli     *mininfo); 	/* minumum penalty      */

/*****************************************************************************/
/* global functions for representing and building puzzling step trees        */
/*****************************************************************************/

/* perform one single puzzling step to produce one intermediate tree */
void onepstep_split(                         /* PStep (intermediate) tree topol: */
         ONEEDGE_SPLIT **edge,          /*   out: new array of edges        */
         int     **edgeofleaf,         /*   out: array of extern edge ptrs */
         unsigned char *quartettopols, /* in: quartetblock with all topols */
         int       Maxspc,             /* in: Number of species (n)        */
         ivector   permutation);       /* in: species permutation (trueID) */

/* perform Numtrial single puzzling steps constructing Numtrial intermediate */
/* trees, sort each of them and extract splits for consensus step            */
void allpstep_split(uli       Numtrial,         /* in: no. psteps to perform       */
              unsigned char *quartetinfo, /* in: quartetblock with all topols*/
              int       Maxspc,           /* in: Number of species (n)       */
              int       fixedorder_optn); /* in: 'fixed' anchored RNG (debug)*/

#endif /* PSTEP_SPLIT_H */

