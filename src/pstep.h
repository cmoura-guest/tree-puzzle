/*
 *   pstep.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */


#ifndef PSTEP_H
#define PSTEP_H

#include<stdio.h>
#include<stdlib.h>
#include"puzzle.h"
#include"util.h"
/* #include"newpstep.h" */

/******************************************************************/
/*  switch puzzling step algorithm                                */
/******************************************************************/

#ifndef PSTEP_RECUR
#	define PSTEP_RECUR
#endif /* PSTEP_RECUR */

#ifdef PSTEP_SPLIT
#	define PSTEPALGNAME "split-based puzzling step algorithm"

#	ifdef PSTEP_ORIG
#		undef PSTEP_ORIG
#	endif /* PSTEP_ORIG */

#	ifdef PSTEP_MRCA
#		undef PSTEP_MRCA
#	endif /* PSTEP_MRCA */

#	ifdef PSTEP_RECUR
#		undef PSTEP_RECUR
#	endif /* PSTEP_RECUR */
#endif /* PSTEP_SPLIT */

#ifdef PSTEP_MRCA
#	define PSTEPALGNAME "mrca-based puzzling step algorithm"

#	ifdef PSTEP_ORIG
#		undef PSTEP_ORIG
#	endif /* PSTEP_ORIG */

#	ifdef PSTEP_SPLIT
#		undef PSTEP_SPLIT
#	endif /* PSTEP_SPLIT */

#	ifdef PSTEP_RECUR
#		undef PSTEP_RECUR
#	endif /* PSTEP_RECUR */
#endif /* PSTEP_MRCA */

#ifdef PSTEP_ORIG
#	define PSTEPALGNAME "original puzzling step algorithm"

#	ifdef PSTEP_MRCA
#		undef PSTEP_MRCA
#	endif /* PSTEP_MRCA */

#	ifdef PSTEP_SPLIT
#		undef PSTEP_SPLIT
#	endif /* PSTEP_SPLIT */

#	ifdef PSTEP_RECUR
#		undef PSTEP_RECUR
#	endif /* PSTEP_RECUR */
#endif /* PSTEP_ORIG */

#ifdef PSTEP_RECUR
#	define PSTEPALGNAME "recursive puzzling step algorithm"

#	ifdef PSTEP_ORIG
#		undef PSTEP_ORIG
#	endif /* PSTEP_ORIG */

#	ifdef PSTEP_MRCA
#		undef PSTEP_MRCA
#	endif /* PSTEP_MRCA */

#	ifdef PSTEP_SPLIT
#		undef PSTEP_SPLIT
#	endif /* PSTEP_SPLIT */
#endif /* PSTEP_RECUR */

/******************************************************************/

#define NOWHERE     0
#define UP          1
#define UPDOWNLEFT  2
#define UPDOWNRIGHT 3
#define DOWNLEFT    4
#define DOWNRIGHT   5
#define STARTING    6

/******************************************************************/

/* tree structure */
typedef struct oneedge {
       	/* pointer to other three edges */
       	struct oneedge *up;
       	struct oneedge *downleft;
       	struct oneedge *downright;
       	int             numedge;    /* number of edge (index) */
       	int             taxon;      /* taxon number */
       	uli             edgeinfo;   /* value of this edge (penalty) */
#	ifdef PSTEP_ORIG
		int    *edgemap;   /* pointer to the local edgemap array */
#	endif /* PSTEP_ORIG */
#	ifdef PSTEP_SPLIT
		uli     penalty;   /* penalty of this edge */
		int    *split;     /* cluster array (up: 0..upsize-1; */
		                   /*   down: maxspc-1..maxspc-downsize */
		int     upsize;    /* size of root-ward cluster */
		int     downsize;  /* size of non-root-ward cluster */
#	endif /* PSTEP_SPLIT */
#	ifdef PSTEP_RECUR
		uli     penalty;   /* penalty of this edge */
		int    *cluster;   /* leaf cluster of edge (0..clsize-1; */
		int     clsize;    /* size of non-root-ward cluster */
		uli     submatrsum;/* sum of all neibor-matr cluster entries */
		uli     allsum;    /* sum of all cluster ext. edge penalty */
#	endif /* PSTEP_RECUR */
#	ifdef PSTEP_MRCA
		uli     penalty;   /* penalty of this edge */
		uli    *penaltyprop; /* penalties to propagated to the leaves */
		int    *cluster;   /* leaf cluster of edge (0..clsize-1; */
		int     clsize;    /* size of non-root-ward cluster */
#	endif /* PSTEP_MRCA */
} ONEEDGE;


/*****************************************************************************/
/* A bit of documentation: (HAS)                                             */
/*****************************************************************************/

/* Tree initialization:
 * --------------------
 *
 * A puzzling step tree is initialized like this:
 *
 *                2
 *         0  +------- C(=2)
 * A(=0) -----+
 *            +------- B(=1)
 *                1
 *
 *
 * more detailed:
 *
 *               A(=0)
 *               [up       =NULL]
 *               [downleft =ptr(1)]
 *               [downright=ptr(2)]
 *  PSTEP_SPLIT:
 *               [upcluster=0]
 *               [downcluster=1,2]
 *               [upsize=1]
 *               [downsize=2]
 *  PSTEP_RECUR, PSTEP_MRCA:
 *               [cluster=t1,t2]
 *               [clsize=2]
 *
 *                    o
 *                    |
 *                    |
 *      +-------------+--------------+
 *      |                            |
 *      |                            |
 *      o                            o
 *
 *   C(=1)                        B(=2)
 *   [up       =ptr(0)]           [up       =ptr(0)]
 *   [downleft =NULL]             [downleft =NULL]
 *   [downright=NULL]             [downright=NULL]
 * PSTEP_SPLIT:
 *   [upcluster=t0,t2]            [upcluster=t0,t1]
 *   [downcluster=t1]             [downcluster=t2]
 *   [upsize=2]                   [upsize=2]
 *   [downsize=1]                 [downsize=1]
 *  PSTEP_RECUR, PSTEP_MRCA:
 *   [cluster=t1]                 [cluster=t2]
 *   [clsize=1]                   [clsize=1]
 *
 *  PSTEP_MRCA:
 *   MRCA [t0,*]  = 0
 *   MRCA [t1,t2] = 0
 *
 *   nextedge = 3
 *   nextleaf = 3
 *   and set according edge maps (_orig, PSTEP_ORIG), 
 *                     split lists (_split, PSTEP_SPLIT),
 *                     cluster lists (_recur, PSTEP_MRCA), or
 *                     MRCA matrix (_mrca, PSTEP_MRCA),
 *
 *
 *
 * Adding leaves to <dockedge> works like this:
 * --------------------------------------------
 *
 *      
 *                                    |
 *                                    | nextedge
 *  |                                 |
 *  | dockedge   insert              /^\
 *  |           ========>           *   *
 * /^\                             /     \
 *                       dockedge /       \ nextedge+1
 *                               /         \ 
 *                              /|         |\ permut[nextleaf]
 *
 */

/*****************************************************************************/
/* internal functions for representing and building puzzling step trees      */
/*****************************************************************************/

/* writes OTU sitting on edge ed (usage: debugging) */
void writeOTU(FILE    *outfp, 		/* output file          */
              int      ed, 		/* edge to subtree      */
              ONEEDGE *edge, 		/* edge array           */
              int     *edgeofleaf, 	/* ext. edge idx array  */
              int      nextedge, 	/* next free edge idx   */
              int      nextleaf,	/* next free leaf idx   */
              int     *column);		/* current screen depth */

/* write tree (usage: debugging) */
void writetree(FILE   *outfp,              /* output file          */
               ONEEDGE *edge,              /* edge array           */
               int     *edgeofleaf,        /* ext. edge idx array  */
               int      nextedge,          /* next free edge idx   */
               int      nextleaf,          /* next free leaf idx   */
               int      rootleaf);         /* root leaf of tree    */

/* clear all edgeinfos */
void resetedgeinfo(ONEEDGE *edge,      	/* edge array           */
                   int      nextedge);	/* next free edge idx   */

/* checks which edge has the lowest edgeinfo
   if there are several edges with the same lowest edgeinfo,
   one of them will be selected randomly */
void minimumedgeinfo(ONEEDGE *edge,        /* edge array           */
                     int     *edgeofleaf,  /* ext. edge idx array  */
                     int      nextedge,    /* next free edge idx   */
                     int      nextleaf,    /* next free leaf idx   */
	             int     *minedges,
	             int     *out_howmany,
                     int     *out_minedge,     /* minimum edge set     */
                     uli     *out_mininfo);    /* minumum penalty      */

/*******************************************************/
/* routines specific for the original O(n^5) algorithm */
/*******************************************************/

/* initialize tree with the following starting configuration (see above) */
void inittree_orig(ONEEDGE **edge,      /* out: new array of edges          */
                   int     **edgeofleaf,/* out: array of external edge ptrs */
                   int      *rootedge,  /* out: rooting edge (=0)           */
                   int       Maxspc,    /* in:  Number of species (n)       */
                   int       Maxbrnch,  /* in:  Number of branches (2n-3)   */
                   int      *nextedge,  /* out: next free edge index (=3)   */
                   int      *nextleaf,  /* out: next free leaf index (=3)   */
                   int      *trueID);

/* update edgemap */
void updateedgemap_orig(int      dockedge,        /* insert here         */
                        ONEEDGE *edge,            /* edge array          */
                        int      nextedge);       /* next free edge idx  */

/* add next leaf on the specified edge */
void addnextleaf_orig(int      dockedge,        /* insert here         */
                      ONEEDGE *edge,            /* edge array          */
                      int     *edgeofleaf,      /* ext. edge idx array */
                      int      rootedge,	/* uppest edge */
                      int      Maxspc,          /* No. of species      */
                      int      Maxbrnch,        /* No. of branches     */
                      int     *in_nextedge,        /* next free edge idx  */
                      int     *in_nextleaf,        /* next free leaf idx  */
                      ivector  permut);

/* free memory (to be called after inittree) */
void freetree_orig(ONEEDGE *edge,		/* edge array          */
                   int     *edgeofleaf, 	/* ext. edge idx array */
                   int      Maxspc);		/* No. of species      */

/* increment all edgeinfo between leaf A and B */
void incrementedgeinfo_orig(int      A, 	/* start leaf of penalty path */
                            int      B,		/* start leaf of penalty path */
                            ONEEDGE *edge,	  /* edge array           */
                            int     *edgeofleaf); /* ext. edge idx array  */

/*********/

/* perform one single puzzling step to produce one intermediate tree */
void onepstep_orig(                    /* PStep (intermediate) tree topol:   */
         ONEEDGE      **edge,          /*   out: new array of edges          */
         int          **edgeofleaf,    /*   out: array of extern edge ptrs   */
         int           *rootleaf,      /*   out: root leaf in topol, starting perm[0]  */
         unsigned char *quartettopols, /* in: quartetblock with all topols   */
         int            Maxspc,        /* in: Number of species (n)          */
         ivector        permut);       /* in: species permutation (trueID)   */


/**********************************************************/
/* routines specific for the split-based O(n^4) algorithm */
/**********************************************************/

/* initialize tree with the following starting configuration (see above) */
void inittree_split(ONEEDGE **edge,      /* out: new array of edges          */
                    int     **edgeofleaf,/* out: array of external edge ptrs */
                    int      *rootedge,  /* out: rooting edge (=0)           */
                    int       Maxspc,    /* in:  Number of species (n)       */
                    int       Maxbrnch,  /* in:  Number of branches (2n-3)   */
                    int      *nextedge,  /* out: next free edge index (=3)   */
                    int      *nextleaf,  /* out: next free leaf index (=3)   */
                    int      *trueID);

#if 0
/* update edgemap */
void updateedgemap_split(int      dockedge,        /* insert here         */
                        ONEEDGE *edge,            /* edge array          */
                        int      nextedge);       /* next free edge idx  */
#endif

/* add next leaf on the specified edge */
void addnextleaf_split(int      dockedge,        /* insert here         */
                       ONEEDGE *edge,            /* edge array          */
                       int     *edgeofleaf,      /* ext. edge idx array */
                       int      rootedge,	/* uppest edge */
                       int      Maxspc,          /* No. of species      */
                       int      Maxbrnch,        /* No. of branches     */
                       int     *in_nextedge,        /* next free edge idx  */
                       int     *in_nextleaf,        /* next free leaf idx  */
                       ivector  permut);

/* free memory (to be called after inittree) */
void freetree_split(ONEEDGE *edge,		/* edge array          */
                    int     *edgeofleaf, 	/* ext. edge idx array */
                    int      Maxspc);		/* No. of species      */

#if 0
/* increment all edgeinfo between leaf A and B */
void incrementedgeinfo_split(int      A, 	/* start leaf of penalty path */
                            int      B,		/* start leaf of penalty path */
                            ONEEDGE *edge,	  /* edge array           */
                            int     *edgeofleaf); /* ext. edge idx array  */
#endif

/*********/

/* perform one single puzzling step to produce one intermediate tree */
void onepstep_split(                    /* PStep (intermediate) tree topol:   */
         ONEEDGE      **edge,          /*   out: new array of edges          */
         int          **edgeofleaf,    /*   out: array of extern edge ptrs   */
         int           *rootleaf,      /*   out: root leaf in topol, starting perm[0]  */
         unsigned char *quartettopols, /* in: quartetblock with all topols   */
         int            Maxspc,        /* in: Number of species (n)          */
         ivector        permut);       /* in: species permutation (trueID)   */


/*****************************************************************************/
/* global functions for representing and building puzzling step trees        */
/*****************************************************************************/

/* perform Numtrial single puzzling steps constructing Numtrial intermediate */
/* trees, sort each of them and extract splits for consensus step            */
void allpstep(uli       Numtrial,         /* in: no. psteps to perform       */
              unsigned char *quartetinfo, /* in: quartetblock with all topols*/
              int       Maxspc,           /* in: Number of species (n)       */
              int       fixedorder_optn); /* in: 'fixed' anchored RNG (debug)*/

#endif /* PSTEP_H */

