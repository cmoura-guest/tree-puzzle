/*
 * consensus.c
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */


#define EXTERN extern
#include "consensus.h"
#ifdef PARALLEL
#	include "ppuzzle.h"
#endif

int ZZZ=0;
/* fprintf(stderr, "ZZZ: %d (%s:%d)\n", ZZZ++, __FILE__, __LINE__); */


/******************************************************************************/
/* functions for computing the consensus tree                                 */
/******************************************************************************/

/* prepare for consensus tree analysis */
void initconsensus()
{
#	if ! PARALLEL
		biparts = new_cmatrix(Maxspc-3, Maxspc);
#	endif /* PARALLEL */

	if (Maxspc % 32 == 0)
		splitlength = Maxspc/32;
	else splitlength = (Maxspc + 32 - (Maxspc % 32))/32;
	numbiparts = 0; /* no pattern stored so far */
	maxbiparts = 0; /* no memory reserved so far */
	splitfreqs = NULL;
	splitpatterns = NULL;
	splitsizes = NULL;
	splitcomptemp = (uli *) calloc((size_t) splitlength, sizeof(uli) );
	if (splitcomptemp == NULL) maerror("splitcomptemp in initconsensus");
} /* initconsensus */

/******************/


/* de-trueID-ed (HAS) */
/* recursive function to get bipartitions */
/* traversal should be optimazable (HAS) */
void makepart(int           i,          /*  */
              int           curribrnch, /* in: current branch in traversal */
              ONEEDGE      *edge,       /* in: tree topology    */
              int          *edgeofleaf, /* in: ext. edge list   */
              cmatrix       biparts,    /* out: split strings, edge by edge */
              int           Maxspc)	/* in: number of species in tree */
{	
	if ( edge[i].downright == NULL || edge[i].downleft == NULL) { 
		biparts[curribrnch][edge[i].taxon] = '*';
		return;
	} else { /* still on inner branch */
		makepart(edge[i].downleft->numedge, curribrnch, edge, edgeofleaf, biparts, Maxspc);
		makepart(edge[i].downright->numedge, curribrnch, edge, edgeofleaf, biparts, Maxspc);
	}
} /* makepart */

/******************/


/* de-trueID-ed (HAS) */
/* compute bipartitions of current puzzling step tree */
/* traversal should be optimazable (HAS) */
void computebiparts(ONEEDGE      *edge,       /* in: tree topology          */
                    int          *edgeofleaf, /* in: ext. edge list         */
                    int           psteprootleaf, /* virtual root from pstep */
                    cmatrix       biparts,    /* out: splits                */
                    int           outgroup,   /* in: outgroup of tree       */
                    int           Maxspc)     /* in: number of taxa in tree */
{
	int i, j, curribrnch;
	
	curribrnch = -1;
	
	for (i = 0; i < Maxspc - 3; i++)
		for (j = 0; j < Maxspc; j++)
			biparts[i][j] = '.';

	for (i = 0; i < Maxbrnch; i++) {   /* (HAS) */
		if (!(edgeofleaf[psteprootleaf] == i
		      || edge[i].downright == NULL
		      || edge[i].downleft == NULL) ) { /* check only inner branches */
			curribrnch++;
			makepart(i, curribrnch, edge, edgeofleaf, biparts, Maxspc);
			
			/* make sure that the root is always a '*' */
			if (biparts[curribrnch][outgroup] == '.') {
				for (j = 0; j < Maxspc; j++) {
					if (biparts[curribrnch][j] == '.')
						biparts[curribrnch][j] = '*';
					else
						biparts[curribrnch][j] = '.';
				}
			}
		}
	}
} /* computebiparts */

/******************/


/* print out the bipartition n of all different splitpatterns */
void fprintfsplit(FILE *fp,		/* outputfile stream */
                uli   n,		/* split number */
                uli  *splitpatterns,	/* splits */
                int   splitlength,	/* number of ulis per split */
                int   Maxspc)		/* number of taxa */
{
	int i, j;	/* counter variables */
	int col;	/* current output column */
	uli z;		/* temporary variable for bit shifting */
	
	col = 0;
	for (i = 0; i < splitlength; i++) {
		z = splitpatterns[n*splitlength + i];
		for (j = 0; j < 32 && col < Maxspc; j++) {
			if (col % 10 == 0 && col != 0) fprintf(fp, " ");
			if (z & 1) fprintf(fp, ".");
			else fprintf(fp, "*");
			z = (z >> 1);
			col++;
		}
	}
} /* printsplit */

/******************/

/* general remarks:

   - every entry in consbiparts is one node of the consensus tree
   - for each node one has to know which taxa and which other nodes
     are *directly* descending from it
   - for every taxon/node number there is a flag that shows
     whether it descends from the node or not
   - '0' means that neither a taxon nor another node with the
         corresponding number decends from the node
     '1' means that the corresponding taxon descends from the node
     '2' means that the corresponding node descends from the node
     '3' means that the corresponding taxon and node descends from the node
*/

/* make new entries for new different bipartitions and count frequencies */
/* internal use: splitcomptemp */
void makenewsplitentries(cmatrix  bip,             /* in: split string vector */
                         int      numspl,          /* in: no. of new splits   */
                         uli    **in_splitpatterns,/* io: known compr splits  */
                         int    **in_splitsizes,   /* io: kn. split sizes: '.'*/
                         uli    **in_splitfreqs,   /* io: kn. split frequences*/
                         uli     *in_numbiparts,   /* io: no. of splits so far*/
	                 uli     *in_maxbiparts,   /* io: reserved memory     */
                         int      Maxspc)          /* in: no. of species      */
{
        int  i, j;          /* count variables */
	int  bpc;           /* split count */
	int  identical;     /* split identical flag */
        int  idflag;        /* split size identical flag */
	int  bpsize;        /* split size: count the '.' */
        uli  nextentry;     /* temp vaiable for split amount numbiparts */
	uli  obpc;          /* split count - old splits */
	uli *splitpatternstmp; /* temp pointer, saves derefs. */
	int *splitsizestmp;    /* temp pointer, saves derefs. */
	uli *splitfreqstmp;    /* temp pointer, saves derefs. */
	uli  maxbipartstmp; /* split memory reserved so far */

	splitpatternstmp = *in_splitpatterns;
	splitsizestmp    = *in_splitsizes;
	splitfreqstmp    = *in_splitfreqs;
	maxbipartstmp    = *in_maxbiparts;

        /* where the next entry would be in splitpatterns */
        nextentry = *in_numbiparts;

        for (bpc = 0; bpc < numspl; bpc++) { /* for every new bipartition */
                /* convert bipartition into a more compact format */
                bpsize = 0;
                for (i = 0; i < splitlength; i++) {
                        splitcomptemp[i] = 0;
                        for (j = 0; j < 32; j++) {
                                splitcomptemp[i] = splitcomptemp[i] >> 1;
                                if (i*32 + j < Maxspc)
                                        if (bip[bpc][i*32 + j] == '.') {
                                                /* set highest bit */
                                                splitcomptemp[i] = (splitcomptemp[i] | 2147483648UL);
                                                bpsize++; /* count the '.' */
                                        }
                        }
                }
                /* compare to the *old* patterns */
                identical = FALSE;
                for (obpc = 0; (obpc < *in_numbiparts) && (!identical); obpc++) {
                        /* compare first partition size */
                        if (splitsizestmp[obpc] == bpsize) idflag = TRUE;
                        else idflag = FALSE;
                        /* if size is identical compare whole partition */
                        for (i = 0; (i < splitlength) && idflag; i++)
                                if (splitcomptemp[i] != splitpatternstmp[obpc*splitlength + i])
                                        idflag = FALSE;
                        if (idflag) identical = TRUE;
                }
                if (identical) { /* if identical increase frequency */
                        splitfreqstmp[2*(obpc-1)]++;
                } else { /* create new entry */
                        if (nextentry == maxbipartstmp) { /* reserve more memory */
                                maxbipartstmp = maxbipartstmp + 2*Maxspc;
                                splitfreqstmp = (uli *) myrealloc(splitfreqstmp,
                                        2*maxbipartstmp * sizeof(uli) );
                                /* 2x: splitfreqstmp contains also an index (sorting!) */
                                if (splitfreqstmp == NULL) maerror("splitfreqstmp in makenewsplitentries");
                                splitpatternstmp = (uli *) myrealloc(splitpatternstmp,
                                        splitlength*maxbipartstmp * sizeof(uli) );
                                if (splitpatternstmp == NULL) maerror("splitpatternstmp in makenewsplitentries");
                                splitsizestmp = (int *) myrealloc(splitsizestmp,
                                        maxbipartstmp * sizeof(int) );
                                if (splitsizestmp == NULL) maerror("splitsizestmp in makenewsplitentries");
                        }
                        splitfreqstmp[2*nextentry] = 1; /* frequency */
                        splitfreqstmp[2*nextentry+1] = nextentry; /* index for sorting */
                        for (i = 0; i < splitlength; i++)
                                splitpatternstmp[nextentry*splitlength + i] = splitcomptemp[i];
                        splitsizestmp[nextentry] = bpsize;
                        nextentry++;
                }
        }

	/* export new values */
        *in_numbiparts = nextentry;
	*in_splitpatterns = splitpatternstmp;
	*in_splitsizes = splitsizestmp;
	*in_splitfreqs = splitfreqstmp;
	*in_maxbiparts = maxbipartstmp;

} /* makenewsplitentries - new */


/***************************************************************************/

/* copy bipartition n of all different splitpatterns to consbiparts[k] */
void copysplit(uli n, uli *splitpatterns, int k, cmatrix consbipartsvec)
{
	int i, j, col;
	uli z;
	
	col = 0;
	for (i = 0; i < splitlength; i++) {
		z = splitpatterns[n*splitlength + i];
		for (j = 0; j < 32 && col < Maxspc; j++) {
			if (z & 1) consbipartsvec[k][col] = '1';
			else       consbipartsvec[k][col] = '0';
			z = (z >> 1);
			col++;
			consbipartsvec[k][Maxspc] = '\0';
		}
	}
} /* copysplit */

/******************/



/* <consbipartsvec> and the quartet topologies from the ML step. Values are  */
/* stored in <qsupparr[splitnum]>                                            */
/* missing parameter: quartetinfo */
void quartsupport(int splitnum, cmatrix consbipartsvec, qsupportarr_t *qsupparr)

{
	int  n;
	int *clusterA;
	int *clusterB;
	int  clustA = 0;
	int  clustB = 0;

	int a1, a2, b1, b2;
	int aa1, aa2, bb1, bb2;
	uli  fullres_pro = 0;
	uli  fullres_con = 0;
	uli  partres_pro = 0;
	uli  partres_con = 0;
	uli  unres = 0;
	uli  missing = 0;
	uli  qsum = 0;

	cvector splitstr;
	unsigned char tmpweight;

	clusterA = calloc((size_t) Maxspc, (sizeof(int)));
	if (clusterA == NULL) maerror("clusterA in quartsupport");
	clusterB = calloc((size_t) Maxspc, (sizeof(int)));
	if (clusterA == NULL) maerror("clusterB in quartsupport");

	splitstr = consbipartsvec[splitnum];

	for(n=0; n<Maxspc; n++) {
		if (splitstr[n] == '0') clusterA[clustA++] = n;
		else                    clusterB[clustB++] = n;
	}

	if ((clustA <= 1) ||  (clustB <= 1)) {
		/* split in outer edge */
		fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR HS1 TO DEVELOPERS\n\n\n");
#		if PARALLEL
			PP_Finalize();
#		endif
		exit(1);
	}

	for (aa1 = 0; aa1 < clustA-1; aa1++)
	  for (aa2 = aa1+1; aa2 < clustA; aa2++)
	     for (bb1 = 0; bb1 < clustB-1; bb1++)
		for (bb2 = bb1+1; bb2 < clustB; bb2++) {
		   a1 = clusterA[aa1];
		   a2 = clusterA[aa2];
		   b1 = clusterB[bb1];
		   b2 = clusterB[bb2];

		   if (a1 < b1) {
		      if (b1 < a2) {
		         if (a2 < b2) { /* a1<b1<a2<b2 */
		             tmpweight = readquartet(a1, b1, a2, b2);
			     qsum++;
			     switch (tmpweight) {
				case 1: fullres_con++; break;
				case 2: fullres_pro++; break;
				case 3: partres_pro++; break;
				case 4: fullres_con++; break;
				case 5: partres_con++; break;
				case 6: partres_pro++; break;
				case 7: unres++; break;
				case 0: missing++; break;
			     }
		         } else { /* a1<b1<b2<a2 */
		             tmpweight = readquartet(a1, b1, b2, a2);
			     qsum++;
			     switch (tmpweight) {
				case 1: fullres_con++; break;
				case 2: fullres_con++; break;
				case 3: partres_con++; break;
				case 4: fullres_pro++; break;
				case 5: partres_pro++; break;
				case 6: partres_pro++; break;
				case 7: unres++; break;
				case 0: missing++; break;
			     }
		         }
		      } else { /* a1<a2<b1<b2 */
		          tmpweight = readquartet(a1, a2, b1, b2);
			  qsum++;
			  switch (tmpweight) {
			     case 1: fullres_pro++; break;
			     case 2: fullres_con++; break;
			     case 3: partres_pro++; break;
			     case 4: fullres_con++; break;
			     case 5: partres_pro++; break;
			     case 6: partres_con++; break;
			     case 7: unres++; break;
			     case 0: missing++; break;
		          }
		      }
		   } else {
		      if (a1 < b2) {
		         if (b2 < a2) { /* b1<a1<b2<a2 */
		             tmpweight = readquartet(b1, a1, b2, a2);
			     qsum++;
			     switch (tmpweight) {
				case 1: fullres_con++; break;
				case 2: fullres_pro++; break;
				case 3: partres_pro++; break;
				case 4: fullres_con++; break;
				case 5: partres_con++; break;
				case 6: partres_pro++; break;
				case 7: unres++; break;
				case 0: missing++; break;
			     }
		         } else { /* b1<a1<a2<b2 */
		             tmpweight = readquartet(b1, a1, a2, b2);
			     qsum++;
			     switch (tmpweight) {
				case 1: fullres_con++; break;
				case 2: fullres_con++; break;
				case 3: partres_con++; break;
				case 4: fullres_pro++; break;
				case 5: partres_pro++; break;
				case 6: partres_pro++; break;
				case 7: unres++; break;
				case 0: missing++; break;
			     }
		         }
		      } else { /* b1<b2<a1<a2 */
		          tmpweight = readquartet(b1, b2, a1, a2);
			  qsum++;
			  switch (tmpweight) {
			     case 1: fullres_pro++; break;
			     case 2: fullres_con++; break;
			     case 3: partres_pro++; break;
			     case 4: fullres_con++; break;
			     case 5: partres_pro++; break;
			     case 6: partres_con++; break;
			     case 7: unres++; break;
			     case 0: missing++; break;
		          }
		      }
		   }
	}

	qsupparr[splitnum].fullres_pro = fullres_pro;
	qsupparr[splitnum].fullres_con = fullres_con;
	qsupparr[splitnum].partres_pro = partres_pro;
	qsupparr[splitnum].partres_con = partres_con;
	qsupparr[splitnum].unres= unres;
	qsupparr[splitnum].missing = missing;
	qsupparr[splitnum].qsum = qsum;

	free(clusterA);
	free(clusterB);
} /* quartsupport */

/******************/



/* compute majority rule consensus tree */
void makeconsensus(uli maxnumtrees, int qsupp_optn)
{
	int i, j, k, n, size, subnode;
	char chari, charj;
	int done;
	int count01, count10, count00, count11, scount;

	/* sort bipartition frequencies */
	qsort(splitfreqs, numbiparts, 2*sizeof(uli), ulicmp);
	/* how many bipartitions are included in the consensus tree */
	consincluded = 0;
	for (i = 0; (uli)i < numbiparts && i == consincluded; i++) {
		if (2*splitfreqs[2*i] > maxnumtrees) consincluded = i + 1;
	}

	/* collect all info about majority rule consensus tree */
	/* the +1 is due to the edge with the root */
	/* consincluded changed to (Maxspc-3) to collect sub-50%-splits */
	/* a maximum of (Maxspc-3) is possible in a Maxspc-tree (HAS) */
	consconfid  = new_ivector(  (Maxspc-3) + 1);
	conssizes   = new_ivector(2*(Maxspc-3) + 2);
	consbiparts = new_cmatrix(  (Maxspc-3) + 1, Maxspc + 1);

	/* old routine (HAS)
	consconfid = new_ivector(consincluded + 1);
	conssizes = new_ivector(2*consincluded + 2);
	consbiparts = new_cmatrix(consincluded + 1, Maxspc);
	*/
	
	for (i = 0; i < consincluded; i++) {
		/* copy partition to consbiparts */
		copysplit(splitfreqs[2*i+1], splitpatterns, i, consbiparts);
		/* frequency in percent (rounded to integer) */
		consconfid[i] = (int) floor(100.0*splitfreqs[2*i]/maxnumtrees + 0.5);
		/* size of partition */
		conssizes[2*i] = splitsizes[splitfreqs[2*i+1]];
		conssizes[2*i+1] = i;
	}

	consfifty = consincluded;
	done = FALSE;
	i = consincluded;

	/* until maximum splits (Maxspc-3) incorporated or */
	/* first incongruence found */
	while ((i < (Maxspc-3)) && !done) { 

		/* copy partition to consbiparts */
		copysplit(splitfreqs[2*i+1], splitpatterns, i, consbiparts);
	
		for (scount = 0; (scount<i) && (!done); scount++) {
			count01 = 0;
			count10 = 0;
			count00 = 0;
			count11 = 0;
			
			for (n = 0; (n<Maxspc) && (!done); n++) {
				if (consbiparts[i][n] != consbiparts[scount][n]) {
					if (consbiparts[i][n] == '0') count01++;
					else                          count10++;
				} else {
					if (consbiparts[i][n] == '0') count00++;
					else                          count11++;
				}
				if ((count01 > 0) && (count10 > 0) && (count00 > 0) && (count11 > 0)) done = TRUE;
			}
		}
		if (!done) {
			/* frequency in percent (rounded to integer) */
			consconfid[i] = (int) floor(100.0*splitfreqs[2*i]/maxnumtrees+ 0.5);
			/* size of partition */
			conssizes[2*i] = splitsizes[splitfreqs[2*i+1]];
			conssizes[2*i+1] = i;
			i++;
		}
	} 
	
	/* last split has same percentage as the first unincorporated, remove */
	if (((uli)(i + 1) < numbiparts) && (splitfreqs[2*i] == splitfreqs[2*(i+1)]))
		done = TRUE;

	/* remove all splits with same percentage as first incongruence */
	if (done) {
		while ((i > 0) && (splitfreqs[2*i] == splitfreqs[2*(i-1)])) 
			i--;
	}
	
	if (conssub50_optn) {
		consincluded = i;
		conscongruent = i;
	} else {
		conscongruent = i;
	}

	for (i = 0; i < Maxspc; i++) consbiparts[consincluded][i] = '1';
	consbiparts[consincluded][outgroup] = '0';
	consconfid[consincluded] = 100;
	conssizes[2*consincluded] = Maxspc - 1;
	conssizes[2*consincluded + 1] = consincluded;

	/* sort bipartitions according to cluster size */
	qsort(conssizes, consincluded + 1, 2*sizeof(int), intcmp);

	qsupportarr = malloc((size_t) (sizeof(qsupportarr_t) * consincluded));
	if (qsupp_optn) {
		for (i = 0; i < consincluded; i++) {
			quartsupport(i, consbiparts, qsupportarr);
		}
	}

	/******************************/
	/* reconstruct consensus tree */
	/******************************/
	for (i = 0; i < consincluded; i++) { /* try every node */
		size = conssizes[2*i]; /* size of current node */
		for (j = i + 1; j < consincluded + 1; j++) {
		
			/* compare only with nodes with more descendants */
			if (size == conssizes[2*j]) continue;
			
			/* check whether node i is a subnode of j */
			subnode = FALSE;
			for (k = 0; k < Maxspc && !subnode; k++) {
				chari = consbiparts[ conssizes[2*i+1] ][k];
				if (chari != '0') {
					charj = consbiparts[ conssizes[2*j+1] ][k];
					if (chari == charj || charj == '3') subnode = TRUE;
				}
			}
			
			/* if i is a subnode of j change j accordingly */
			if (subnode) {
				/* remove subnode i from j */
				for (k = 0; k < Maxspc; k++) {
					chari = consbiparts[ conssizes[2*i+1] ][k];
					if (chari != '0') {
						charj = consbiparts[ conssizes[2*j+1] ][k];
						if (chari == charj)
							consbiparts[ conssizes[2*j+1] ][k] = '0';
						else if (charj == '3') {
							if (chari == '1')
								consbiparts[ conssizes[2*j+1] ][k] = '2';
							else if (chari == '2')
								consbiparts[ conssizes[2*j+1] ][k] = '1';
							else {
								/* Consensus tree [1] */
								fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR H TO DEVELOPERS\n\n\n");
#								if PARALLEL
									PP_Finalize();
#								endif
								exit(1);
							}	
						} else {
							/* Consensus tree [2] */
							fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR I TO DEVELOPERS\n\n\n");
#							if PARALLEL
								PP_Finalize();
#							endif
							exit(1);
						}
					}
				}
				/* add link to subnode i in node j */
				charj = consbiparts[ conssizes[2*j+1] ][ conssizes[2*i+1] ];
				if (charj == '0')
					consbiparts[ conssizes[2*j+1] ][ conssizes[2*i+1] ] = '2';
				else if (charj == '1')
					consbiparts[ conssizes[2*j+1] ][ conssizes[2*i+1] ] = '3';
				else {
					/* Consensus tree [3] */
					fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR J TO DEVELOPERS\n\n\n");
#					if PARALLEL
						PP_Finalize();
#					endif
					exit(1);
				}
			}
		}
	}
} /* makeconsensus */

/******************/


/* prototype for recursion */
void writenode(FILE          *treefile,    /* in: output stream */
               int            node,        /* current node */
               int            qsupp_optn,  /* 'print quartet support' flag */
               qsupportarr_t *qsupparr,    /* quartet support values */
               int           *column);     /* current line position */

/* write node (writeconsensustree) */
/* missing: column */
void writenode(FILE          *treefile,    /* in: output stream */
               int            node,        /* current node */
               int            qsupp_optn,  /* 'print quartet support' flag */
               qsupportarr_t *qsupparr,    /* quartet support values */
               int           *column)      /* current line position */
{
	int i, first;
	
	fprintf(treefile, "(");
	(*column)++;
	/* write descending nodes */
	first = TRUE;
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '2' ||
		consbiparts[node][i] == '3') {
			if (first) first = FALSE;
			else {
				fprintf(treefile, ",");
				(*column)++;
			}
			if ((*column) > 60) {
				(*column) = 2;
				fprintf(treefile, "\n");
			}
			/* write node i */
			writenode(treefile, i, qsupp_optn, qsupparr, column);

			if (qsupp_optn) {
				/* reliability value as internal label */
				fprintf(treefile, "%d", consconfid[i]);
				(*column) += 3;
#if 0
				fprintf(treefile, "\"bs=%d|", consconfid[i]);
				(*column) += 7;

				fprintf(treefile, "f+=%.0f|", (100.0*qsupparr[i].fullres_pro/qsupparr[i].qsum));
				(*column) += 7;
				fprintf(treefile, "p+=%.0f|", (100.0*qsupparr[i].partres_pro/qsupparr[i].qsum));
				(*column) += 7;
				fprintf(treefile, "f-=%.0f|", (100.0*qsupparr[i].fullres_con/qsupparr[i].qsum));
				(*column) += 7;
				fprintf(treefile, "p-=%.0f|", (100.0*qsupparr[i].partres_con/qsupparr[i].qsum));
				(*column) += 7;
				fprintf(treefile, "ur=%.0f", (100.0*qsupparr[i].unres/qsupparr[i].qsum));
				(*column) += 6;
				if (qsupparr[i].missing > 0) {
					fprintf(treefile, "|md=%.0f", (100.0*qsupparr[i].missing/qsupparr[i].qsum));
					(*column) += 7;
				}
				fprintf(treefile, "/%ld\"", qsupparr[i].qsum);
				(*column) += 5;
#endif
			} else {
				/* reliability value as internal label */
				fprintf(treefile, "%d", consconfid[i]);
				(*column) += 3;
			}
		}
	}
	/* write descending taxa */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '1' ||
		consbiparts[node][i] == '3') {
			if (first) first = FALSE;
			else {
				fprintf(treefile, ",");
				(*column)++;
			}
			if ((*column) > 60) {
				(*column) = 2;
				fprintf(treefile, "\n");
			}
			(*column) += fputid(treefile, i);
		}
	}
	fprintf(treefile, ")");
	(*column)++;
} /* writenode */

/******************/



/* write consensus tree */
/* missing: consbiparts, consincluded, consconfid,  */
void writeconsensustree(FILE          *treefile,   /* in: output stream */
                        int            qsupp_optn, /* 'print quartsupp' flag */
                        qsupportarr_t *qsupparr)   /* quartet support values */
{
	int i, first;
	int column;
	

	column = 1;
	fprintf(treefile, "(");
	column += fputid(treefile, outgroup) + 2;
	fprintf(treefile, ",");
	/* write descending nodes */
	first = TRUE;
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '2' || consbiparts[consincluded][i] == '3') {
			if (first) first = FALSE;
			else {
				fprintf(treefile, ",");  /* first (outgroup) has ',' already */
				column++;
			}
			if (column > 60) {
				column = 2;
				fprintf(treefile, "\n");
			}
			/* write node i */
			writenode(treefile, i, qsupp_optn, qsupparr, &column);

			if (qsupp_optn) {
				/* reliability value as internal label */
				fprintf(treefile, "%d", consconfid[i]);
				column += 3;
#if 0
				fprintf(treefile, "\"bs=%d|", consconfid[i]);
				column += 7;

				fprintf(treefile, "f+=%.0f|", (100.0*qsupparr[i].fullres_pro/qsupparr[i].qsum));
				column += 7;
				fprintf(treefile, "p+=%.0f|", (100.0*qsupparr[i].partres_pro/qsupparr[i].qsum));
				column += 7;
				fprintf(treefile, "f-=%.0f|", (100.0*qsupparr[i].fullres_con/qsupparr[i].qsum));
				column += 7;
				fprintf(treefile, "p-=%.0f|", (100.0*qsupparr[i].partres_con/qsupparr[i].qsum));
				column += 7;
				fprintf(treefile, "ur=%.0f", (100.0*qsupparr[i].unres/qsupparr[i].qsum));
				column += 6;
				if (qsupparr[i].missing > 0) {
					fprintf(treefile, "|md=%.0f", (100.0*qsupparr[i].missing/qsupparr[i].qsum));
					column += 7;
				}
				fprintf(treefile, "/%ld\"", qsupparr[i].qsum);
				column += 5;
#endif
			} else {
				/* reliability value as internal label */
				fprintf(treefile, "%d", consconfid[i]);
				column += 3;
			}
		}
	}
	/* write descending taxa */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '1' ||
		consbiparts[consincluded][i] == '3') {
			if (first) first = FALSE;
			else {
				fprintf(treefile, ",");
				column++;
			}
			if (column > 60) {
				column = 2;
				fprintf(treefile, "\n");
			}
			column += fputid(treefile, i);
		}
	}
	fprintf(treefile, ");\n");
} /* writeconsensustree */

/******************/


/* prototype for recursion */
void nodecoordinates(int node);

/* establish node coordinates (plotconsensustree) */
void nodecoordinates(int node)
{
	int i, ymin, ymax, xcoordinate;

	/* first establish coordinates of descending nodes */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '2' ||
		    consbiparts[node][i] == '3') 
			nodecoordinates(i);
	}
	
	/* then establish coordinates of descending taxa */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '1' ||
		    consbiparts[node][i] == '3') {
			/* y-coordinate of taxon i */
			ycortax[i] = ytaxcounter;
			ytaxcounter = ytaxcounter - 2;
		}
	}
	
	/* then establish coordinates of this node */
	ymin = 2*Maxspc - 2;
	ymax = 0;
	xcoordinate = 0;
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '2' ||
		    consbiparts[node][i] == '3') {
			if (ycor[i] > ymax) ymax = ycor[i];
			if (ycor[i] < ymin) ymin = ycor[i];
			if (xcor[i] > xcoordinate) xcoordinate = xcor[i];
		}
	}
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '1' ||
		    consbiparts[node][i] == '3') {
			if (ycortax[i] > ymax) ymax = ycortax[i];
			if (ycortax[i] < ymin) ymin = ycortax[i];
		}
	}
	ycormax[node] = ymax;
	ycormin[node] = ymin;
	ycor[node] = (int) floor(0.5*(ymax + ymin) + 0.5);
	if (xcoordinate == 0) xcoordinate = 9;
	xcor[node] = xcoordinate + 4;
} /* nodecoordinates */

/******************/


/* prototype for recursion */
void drawnode(int node, int xold);

/* drawnode  (plotconsensustree) */
void drawnode(int node, int xold)
{
	int i, j;
	char buf[4];
	
	/* first draw vertical line */
	for (i = ycormin[node] + 1; i < ycormax[node]; i++)
		treepict[xcor[node]][i] = ':';
		
	/* then draw descending nodes */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '2' ||
		    consbiparts[node][i] == '3') 
			drawnode(i, xcor[node]);
	}
	
	/* then draw descending taxa */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[node][i] == '1' ||
		    consbiparts[node][i] == '3') {
			treepict[xcor[node]][ycortax[i]] = ':';
			for (j = xcor[node] + 1; j < xsize-10; j++)
				treepict[j][ycortax[i]] = '-';
			for (j = 0; j < 10; j++)
				treepict[xsize-10+j][ycortax[i]] = Identif[i][j];	
		}
	}
	
	/* then draw internal edge with consensus value */
	treepict[xold][ycor[node]] = ':';
	treepict[xcor[node]][ycor[node]] = ':';
	for (i = xold + 1; i < xcor[node]-3; i++)
		treepict[i][ycor[node]] = '-';
	sprintf(buf, "%d", consconfid[node]);
	if (consconfid[node] == 100) {
		treepict[xcor[node]-3][ycor[node]] = buf[0];
		treepict[xcor[node]-2][ycor[node]] = buf[1];
		treepict[xcor[node]-1][ycor[node]] = buf[2];	
	} else {
		treepict[xcor[node]-3][ycor[node]] = '-';
		treepict[xcor[node]-2][ycor[node]] = buf[0];
		treepict[xcor[node]-1][ycor[node]] = buf[1];
	}
} /* drawnode */

/******************/


/* plot consensus tree */
void plotconsensustree(FILE *plotfp)
{
	int i, j, yroot, startree;

	/* star tree or no star tree */
	if (consincluded == 0) {
		startree = TRUE;
		consincluded = 1; /* avoids problems with malloc */
	} else
		startree = FALSE;
	
	/* memory for x-y-coordinates of each bipartition */
	xcor = new_ivector(consincluded);
	ycor = new_ivector(consincluded);
	ycormax = new_ivector(consincluded);
	ycormin = new_ivector(consincluded);
	if (startree) consincluded = 0; /* avoids problems with malloc */

	/* y-coordinates of each taxon */
	ycortax = new_ivector(Maxspc);
	ycortax[outgroup] = 0;
	
	/* establish coordinates */
	ytaxcounter = 2*Maxspc - 2;
	
	/* first establish coordinates of descending nodes */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '2' ||
		    consbiparts[consincluded][i] == '3') 
			nodecoordinates(i);
	}
	
	/* then establish coordinates of descending taxa */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '1' ||
		    consbiparts[consincluded][i] == '3') {
			/* y-coordinate of taxon i */
			ycortax[i] = ytaxcounter;
			ytaxcounter = ytaxcounter - 2;
		}
	}

	/* then establish length of root edge and size of whole tree */
	yroot = 0;
	xsize = 0;
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '2' ||
		    consbiparts[consincluded][i] == '3') {
			if (ycor[i] > yroot) yroot = ycor[i];
			if (xcor[i] > xsize) xsize = xcor[i];
		}
	}
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '1' ||
		    consbiparts[consincluded][i] == '3') {
			if (ycortax[i] > yroot) yroot = ycortax[i];
		}
	}
	if (xsize == 0) xsize = 9;
	/* size in x direction inclusive one blank on the left */
	xsize = xsize + 6; 
	
	/* change all x-labels so that (0,0) is down-left */
	for (i = 0; i < consincluded; i++)
		xcor[i] = xsize-1-xcor[i];
	
	/* draw tree */
	treepict = new_cmatrix(xsize, 2*Maxspc-1);
	for (i = 0; i < xsize; i++)
		for (j = 0; j < 2*Maxspc-1; j++)
			treepict[i][j] = ' ';
	
	/* draw root */
	for (i = 1; i < yroot; i++)
		treepict[1][i] = ':';
	treepict[1][0] = ':';
	for (i = 2; i < xsize - 10; i++)
		treepict[i][0] = '-';
	for (i = 0; i < 10; i++)
		treepict[xsize-10+i][0] = Identif[outgroup][i];
	
	/* then draw descending nodes */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '2' ||
		    consbiparts[consincluded][i] == '3') 
			drawnode(i, 1);
	}
	
	/* then draw descending taxa */
	for (i = 0; i < Maxspc; i++) {
		if (consbiparts[consincluded][i] == '1' ||
		    consbiparts[consincluded][i] == '3') {
			treepict[1][ycortax[i]] = ':';
			for (j = 2; j < xsize-10; j++)
				treepict[j][ycortax[i]] = '-';
			for (j = 0; j < 10; j++)
				treepict[xsize-10+j][ycortax[i]] = Identif[i][j];	
		}
	}
	
	/* plot tree */
	for (i = 2*Maxspc-2; i > -1; i--) {
		for (j = 0; j < xsize; j++)
			fputc(treepict[j][i], plotfp);
		fputc('\n', plotfp);
	}	
	
	free_ivector(xcor);
	free_ivector(ycor);
	free_ivector(ycormax);
	free_ivector(ycormin);
	free_ivector(ycortax);
	free_cmatrix(treepict);
} /* plotconsensustree */




