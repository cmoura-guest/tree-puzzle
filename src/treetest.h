/*
 * treetest.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

#ifndef TREETEST_H
#define TREETEST_H

#include <util.h>

/* minimum log-lh difference to best log-lh to be different */
#define MINKHDIFF 0.005  


/*****************************************************************************/
/* ELW/SR Test (Expected Likelihood Weights, Strimmer & Rambaut, 2002)       */
/*****************************************************************************/
void elw_test(ivector  Alias,
              dmatrix  allsites,
              int      numutrees,
              int      numsites,
              double   siglevel,
              int      numboots,
              ivector *elw_test_passed,
              dvector *elw_Support);

/*****************************************************************************/
/* SH Test (Shimodaira & Hasegawa, 1999)                                     */
/*****************************************************************************/
void sh_test(ivector  Alias,		/* site pattern translat array */
             dmatrix  allsites,		/* tree site log-likelihoods */
             int      numutrees,	/* number of trees */
             int      numsites,		/* number of sites */
             double   siglevel,		/* significance level to test */
             int      numboots,		/* number of bootstraps to draw */
             ivector *sh_test_passed,	/* has tree passed tests */
             dvector *sh_Pval);		/* and p-value */


/* print tree statistics */
void printtreestats(FILE *ofp,
			dvector ulkl,
			dvector ulklc,
			int numutrees,
			dmatrix  allsites,	/* tree site log-likelihoods */
			dmatrix  allsitesc,	/* tree site log-likelihoods */
			ivector  Alias,		/* link site -> corresponding site pattern */
			int Maxsite,
			int Numptrn,
			ivector Weight,
			int compclock);


#endif /* TREETEST_H */

