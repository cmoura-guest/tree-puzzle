/*
 * pstep-deglob.h (obsolete)
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

#ifndef PSTEP_ORIG_H
#define PSTEP_ORIG_H

#include<stdio.h>
#include<stdlib.h>
#include"puzzle.h"
#include"util.h"
/* #include"newpstep.h" */
#include<stdlib.h>

#ifndef ONEEDGE_DEFINED
#define ONEEDGE_DEFINED
	/* tree structure */
	typedef struct oneedge {
        	/* pointer to other three edges */
        	struct oneedge *up;
        	struct oneedge *downleft;
        	struct oneedge *downright;
        	int numedge;    /* number of edge (index) */
        	uli edgeinfo;   /* value of this edge (penalty) */
        	int *edgemap;   /* pointer to the local edgemap array */
	} ONEEDGE_ORIG;
#endif /* ! ONEEDGE_DEFINED */

EXTERN int *edgeofleaf_orig;      /* vector with edge number of all leaves */
EXTERN int minedge_orig;          /* edge with minimum edgeinfo */
EXTERN int nextedge_orig;         /* number of edges in the current tree */
EXTERN int nextleaf_orig;         /* next leaf to add to tree */

EXTERN ONEEDGE_ORIG *edge_orig;   /* vector with all the edges of the tree */
uli *splitcomp_orig;       /* bipartition storage */
uli *splitfreqs_orig;      /* frequencies of all different splits of all trees */
uli *splitpatterns_orig;   /* all different splits of all trees */

#if 0
EXTERN int Maxbrnch;         /* 2*Maxspc - 3 */
EXTERN int Maxspc;           /* Maxspc */
#endif

ivector trueID_orig;
int column_orig;
uli mininfo_orig;          /* value of edgeinfo on minedge */


/*****************************************************************************/
/* functions for representing and building puzzling step trees               */
/*****************************************************************************/

/* initialize tree with the following starting configuration
 *
 *                2
 *         0  +------- C(=2)
 * A(=0) -----+
 *            +------- B(=1)
 *                1
 *
 *
 *               A(=0)
 *               [up       =NULL]
 *               [downleft =1]
 *               [downright=2]
 *
 *                    o
 *                    |
 *                    |
 *      +-------------+--------------+
 *      |                            |
 *      |                            |
 *      o                            o
 *
 *   C(=1)                        B(=2)
 *   [up       =0]                [up       =0]
 *   [downleft =NULL]             [downleft =NULL]
 *   [downright=NULL]             [downright=NULL]
 *
 *   nextedge = 3
 *   nextleaf = 3
 *   and set according edge maps
 *
 */
void inittree_orig(ONEEDGE_ORIG **edge,	/* out: new array of edges          */
                   int     **edgeofleaf,/* out: array of external edge ptrs */
                   int       Maxspc, 	/* in:  Number of species (n)       */
                   int       Maxbrnch, 	/* in:  Number of branches (2n-3)   */
                   int      *nextedge, 	/* out: next free edge index (=3)   */
                   int      *nextleaf);	/* out: next free leaf index (=3)   */

/* add next leaf on the specified edge */
void addnextleaf_orig(int      dockedge,	/* insert here         */
                      ONEEDGE_ORIG *edge, 	/* edge array          */
                      int     *edgeofleaf, 	/* ext. edge idx array */
                      int      Maxspc, 		/* No. of species      */
                      int      Maxbrnch, 	/* No. of branches     */
                      int     *nextedge, 	/* next free edge idx  */
                      int     *nextleaf);	/* next free leaf idx  */

/* free memory (to be called after inittree) */
void freetree_orig(ONEEDGE_ORIG *edge,		/* edge array          */
                   int     *edgeofleaf, 	/* ext. edge idx array */
                   int      Maxspc);		/* No. of species      */

/* writes OTU sitting on edge ed */
void writeOTU_orig(FILE    *outfp, 		/* output file          */
                   int      ed, 		/* edge to subtree      */
                   ONEEDGE_ORIG *edge, 		/* edge array           */
                   int     *edgeofleaf, 	/* ext. edge idx array  */
                   int      nextedge, 		/* next free edge idx   */
                   int      nextleaf,		/* next free leaf idx   */
                   int     *column,		/* current screen depth */
                   int     *trueID);		/* species permutation  */

/* write tree */
void writetree_orig(FILE   *outfp,		/* output file          */
                   ONEEDGE_ORIG *edge, 		/* edge array           */
                   int     *edgeofleaf, 	/* ext. edge idx array  */
                   int      nextedge, 		/* next free edge idx   */
                   int      nextleaf,		/* next free leaf idx   */
                   int     *trueID);		/* species permutation  */

/* clear all edgeinfos */
void resetedgeinfo_orig(ONEEDGE_ORIG *edge, 	/* edge array           */
                        int      nextedge);	/* next free edge idx   */

/* increment all edgeinfo between leaf A and B */
void incrementedgeinfo_orig(int      A, 	/* start leaf of penalty path */
                            int      B,		/* start leaf of penalty path */
                            ONEEDGE_ORIG *edge,	  /* edge array           */
                            int     *edgeofleaf); /* ext. edge idx array  */

/* checks which edge has the lowest edgeinfo
   if there are several edges with the same lowest edgeinfo,
   one of them will be selected randomly */
void minimumedgeinfo_orig(ONEEDGE_ORIG *edge, 	/* edge array           */
                          int     *edgeofleaf, 	/* ext. edge idx array  */
                          int      nextedge,	/* next free edge idx   */ 
                          int      nextleaf,	/* next free leaf idx   */
                          int     *minedge, 	/* minimum edge set     */
                          uli     *mininfo); 	/* minumum penalty      */

#endif /* PSTEP_ORIG_H */

