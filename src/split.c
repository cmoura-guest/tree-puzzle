/*
 * split.c (obsolete)
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

/* initialize tree with the following starting configuration (see pstep.h) */
void inittree_split(ONEEDGE **edge,      /* out: new array of edges          */
                    int     **edgeofleaf,/* out: array of external edge ptrs */
                    int      *rootleaf,  /* out: rooting leaf (=trueID[0])   */
                    int       Maxspc,    /* in:  Number of species (n)       */
                    int       Maxbrnch,  /* in:  Number of branches (2n-3)   */
                    int      *nextedge,  /* out: next free edge index (=3)   */
                    int      *nextleaf,  /* out: next free leaf index (=3)   */
                    int      *trueID)
{
	int i, j;
	ONEEDGE *tmpedge;
	int          *tmpedgeofleaf;

	/* allocate the memory for the whole tree */

	/* allocate memory for vector with all the edges of the tree */
	tmpedge = (ONEEDGE *) calloc((size_t) Maxbrnch, sizeof(ONEEDGE) );
	if (tmpedge == NULL) maerror("edge in inittree");
	*edge = tmpedge;

	/* allocate memory for tmpvector with edge numbers of leaves */
	tmpedgeofleaf = (int *) calloc((size_t) Maxspc, sizeof(int) );
	if (tmpedgeofleaf == NULL) maerror("edgeofleaf in inittree");
	for (j = 0; j < Maxspc; j++) tmpedgeofleaf[j]=-1;
	*edgeofleaf = tmpedgeofleaf;

	/* allocate memory for all the edges the split map */
	for (i = 0; i < Maxbrnch; i++) {
		(tmpedge)[i].split = (int *) calloc((size_t) Maxspc, sizeof(int) );
		if (tmpedge[i].split == NULL) maerror("split array in inittree");
		(tmpedge)[i].taxon = -1;
		for (j = 0; j < Maxspc; j++) (tmpedge)[i].split[j]=-1;

		/* number all edges */
		tmpedge[i].numedge = i;
	}

	/* initialize tree */

	*nextedge = 3;
	*nextleaf = 3;

	/* init split vectors: */
	/*   for edge 0 */
	(tmpedge[0].split)[0]        = trueID[0]; /* leaf 0 above this edge */
	 tmpedge[0].upsize           = 1;
	(tmpedge[0].split)[Maxspc-1] = trueID[1]; /* leaf 0 below this edge */
	(tmpedge[0].split)[Maxspc-2] = trueID[2]; /* leaf 0 below this edge */
	 tmpedge[0].downsize         = 2;

	/*   for edge 1 */
	(tmpedge[1].split)[0]        = trueID[0]; /* leaf 0 above this edge */
	(tmpedge[1].split)[1]        = trueID[2]; /* leaf 0 above this edge */
	 tmpedge[1].upsize           = 2;
	(tmpedge[1].split)[Maxspc-1] = trueID[1]; /* leaf 0 below this edge */
	 tmpedge[1].downsize         = 1;

	/*   for edge 2 */
	(tmpedge[2].split)[0]        = trueID[0]; /* leaf 0 above this edge */
	(tmpedge[2].split)[1]        = trueID[1]; /* leaf 0 above this edge */
	 tmpedge[2].upsize           = 2;
	(tmpedge[2].split)[Maxspc-1] = trueID[2]; /* leaf 0 below this edge */
	 tmpedge[2].downsize         = 1;


	/* interconnection */
	tmpedge[0].up        = NULL;
	tmpedge[0].downleft  = &tmpedge[1];
	tmpedge[0].downright = &tmpedge[2];

	tmpedge[1].up        = &tmpedge[0];
	tmpedge[1].downleft  = NULL;
	tmpedge[1].downright = NULL;

	tmpedge[2].up = &tmpedge[0];
	tmpedge[2].downleft  = NULL;
	tmpedge[2].downright = NULL;

	/* taxon IDs of leaves */
	tmpedge[0].taxon = trueID[0];
	tmpedge[1].taxon = trueID[1];
	tmpedge[2].taxon = trueID[2];

	/* edges of leaves */
	tmpedgeofleaf[trueID[0]] = 0;
	tmpedgeofleaf[trueID[1]] = 1;
	tmpedgeofleaf[trueID[2]] = 2;

	/* uppest edge =: root edge*/
	*rootleaf = trueID[0];

} /* inittree_split */

/******************/


/* free memory (to be called after inittree) */
void freetree_split(ONEEDGE *edge,               /* edge array          */
                   int      *edgeofleaf,         /* ext. edge idx array */
                   int       Maxspc)             /* No. of species      */
{
	int i;

	/* free split vectors */
	for (i = 0; i < 2 * Maxspc - 3; i++) {
		free(edge[i].split);
	}

	/* free tree topology */
	free(edge);

	/* free external edge lookup vector */
	free(edgeofleaf);

} /* freetree_split */


/******************/


/* check edge splits (debugging) */
void check_split(
                       ONEEDGE *edge,       /* edge array                   */
                       int     Maxspc,      /* No. of species               */
                       int    *edgeofleaf,  /* ext. edge idx array          */
                       int     nextedge, /* next free edge idx           */
                       int     nextleaf, /* next free leaf idx           */
                       int    *permut)
{
	int n,m;

	fprintf(stderr, "Permutation: ");
	for (m = 0; m<nextleaf; m++)
		fprintf(stderr, "%2d:%-2d ", m, permut[m]);
	fprintf(stderr, "+ %2d:%-2d\n", m, permut[m]);


	for (m = 0; m < (nextedge); m++) {
		if (edge[m].upsize + edge[m].downsize != nextleaf) {
			fprintf(stderr, "EEE[%2d]%c(%2d)    r<%d | %d>   %d != %d>\n", 
				m, (edge[m].taxon >= 0) ? 'e' : 'i', 
				edge[m].taxon, 
				edge[m].upsize, edge[m].downsize, 
				edge[m].upsize+edge[m].downsize, 
				nextleaf);
			fprintf(stderr, "        UP:");
			for (n = 0; n<edge[m].upsize; n++)
				fprintf(stderr, "%d, ", edge[m].split[n]);
			fprintf(stderr, " | DOWN:");
			for (n = Maxspc - edge[m].downsize; n<Maxspc; n++)
				fprintf(stderr, "%d, ", edge[m].split[n]);
			fprintf(stderr, "\n");
		} else {
			fprintf(stderr, "EEE[%2d]%c(%2d)    r<%d | %d>   %d == %d>     OK\n", 
				m, (edge[m].taxon >= 0) ? 'e' : 'i', 
				edge[m].taxon, 
				edge[m].upsize, edge[m].downsize, 
				edge[m].upsize+edge[m].downsize, 
				nextleaf);
		}
	}
} /* check_split */

/******************/


/* update splits after insertion */
void updatesplit_split(ONEEDGE *curredge,    /* current edge                 */
                       int      wherefrom,   /* direction to last curr. edge */
                       int      newleaf,     /* leaf to add to splits        */
                       ONEEDGE *edge,       /* edge array                   */
                       int      Maxspc,      /* No. of species               */
                       int      Maxbrnch,    /* No. of branches              */
                       int     *edgeofleaf,  /* ext. edge idx array          */
                       int      in_nextedge, /* next free edge idx           */
                       int      in_nextleaf) /* next free leaf idx           */
{
	switch (wherefrom) {
		case UP:
			curredge->split[(curredge->upsize)++] = newleaf; /* leaf is up */
			if (curredge->downright != NULL) { /* not leaf */
				updatesplit_split(curredge->downright, UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				updatesplit_split(curredge->downleft, UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
			} else { /* nothing to do */
				return;
			}
			break;
		case DOWNLEFT:
			curredge->split[Maxspc - ++(curredge->downsize)] = newleaf; /* leaf is down */
			if (curredge->downright != NULL) { /* not leaf */
				updatesplit_split(curredge->downright, UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
			}

			if (curredge->up != NULL) { /* not root */
				if ((curredge->up)->downright == curredge) {
					updatesplit_split(curredge->up, DOWNRIGHT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				} else {
					updatesplit_split(curredge->up, DOWNLEFT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				}
			}
			break;
		case DOWNRIGHT:
			curredge->split[Maxspc - ++(curredge->downsize)] = newleaf; /* leaf is down */
			if (curredge->downright != NULL) { /* not leaf */
				updatesplit_split(curredge->downleft, UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
			}

			if ((curredge->up) != NULL) { /* not root */
				if ((curredge->up)->downright == curredge) {
					updatesplit_split(curredge->up, DOWNRIGHT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				} else {
					updatesplit_split(curredge->up, DOWNLEFT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				}
			}
			break;
	}
} /* updatesplit_split */

/******************/


/* add next leaf on the specified edge */
void updatetreesplits_split(int      dockedge,    /* dockedge                */
                            int      newleaf,     /* leaf to add to splits   */
                            ONEEDGE *edge,        /* edge array              */
                            int      Maxspc,      /* No. of species          */
                            int     *edgeofleaf,  /* ext. edge idx array     */
                            int      in_nextedge, /* next free edge idx      */
                            int      in_nextleaf, /* next free leaf idx      */
                            ivector  permut)
{
	int      n;		/* counter */
        int      wherefrom;	/* direction to last edge  */
	ONEEDGE *curredge;	/* edge array              */

	/* copy clusters from dockedge to in_nextedge */
	/*   up-cluster: upsize-1 .. 0 */
	for (n=(edge[dockedge].upsize) - 1; n >= 0; n--) {
		(edge[in_nextedge]).split[n] = (edge[dockedge]).split[n];
	}
	(edge[in_nextedge]).upsize = (edge[dockedge]).upsize;

	/*   down-cluster: downsize .. Maxspc-1 */
	for (n=Maxspc - (edge[dockedge]).downsize; n < Maxspc; n++) {
		(edge[in_nextedge]).split[n] = (edge[dockedge]).split[n];
	}
	(edge[in_nextedge]).downsize = (edge[dockedge]).downsize;

	/* add the new taxon to in_nextedge */
	(edge[in_nextedge]).split[Maxspc - ++((edge[in_nextedge]).downsize)] 
		= permut[in_nextleaf];


	/* setup clusters for new external edge (in_nextedge+1) */
	/*   up-cluster: all taxa in tree */
	for (n=0; n < in_nextleaf; n++) {
		(edge[in_nextedge+1]).split[n] = permut[n];
	}
	(edge[in_nextedge+1]).upsize = in_nextleaf;

	/*   down-cluster: the new taxon itself */
	(edge[in_nextedge+1]).split[Maxspc-1] = permut[in_nextleaf];
	(edge[in_nextedge+1]).downsize = 1;

	curredge = &(edge[in_nextedge]);
	if (curredge->up != NULL) { /* not root */
		if (((curredge->up)->downright) == curredge) {
			wherefrom = DOWNRIGHT;
		} else {
			wherefrom = DOWNLEFT;
		}

		/* climbing up from insertion point */
		updatesplit_split(curredge->up, wherefrom, newleaf,
				edge, Maxspc, Maxbrnch, edgeofleaf, 
				in_nextedge, in_nextleaf);
	}

	/* climbing down to dockedge from insertion point */
	updatesplit_split(&(edge[dockedge]), UP, newleaf,
				edge, Maxspc, Maxbrnch, edgeofleaf, 
				in_nextedge, in_nextleaf);


} /* updatetreesplits_split */

/******************/


/* add next leaf on the specified edge */
void addnextleaf_split(int      dockedge,        /* insert here         */
                       ONEEDGE *edge,            /* edge array          */
                       int     *edgeofleaf,      /* ext. edge idx array */
                       int      rootleaf,	 /* uppest leaf */
                       int      Maxspc,          /* No. of species      */
                       int      Maxbrnch,        /* No. of branches     */
                       int     *in_nextedge,        /* next free edge idx  */
                       int     *in_nextleaf,        /* next free leaf idx  */
                       ivector  permut)
{
	int nextedge;
	int nextleaf;

	nextedge=*in_nextedge;
	nextleaf=*in_nextleaf;

	if (dockedge >= nextedge) {
		/* Trying to add leaf nextleaf to nonexisting edge dockedge */
		fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR F TO DEVELOPERS\n\n\n");
#		if PARALLEL
			PP_Finalize();
#		endif
		exit(1);
	}

	if (nextleaf >= Maxspc) {
		/* Trying to add leaf nextleaf to a tree with Maxspc leaves */
		fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR G TO DEVELOPERS\n\n\n");
#		if PARALLEL
			PP_Finalize();
#		endif
		exit(1);
	}

	/* necessary change in edgeofleaf if dockedge == root edge */
	if (edgeofleaf[rootleaf] == dockedge) 
		edgeofleaf[rootleaf] = nextedge;

	/* adding nextedge to the tree */
	edge[nextedge].up = edge[dockedge].up;
	edge[nextedge].downleft = &edge[dockedge];
	edge[nextedge].downright = &edge[nextedge+1];
	edge[dockedge].up = &edge[nextedge];

	/* if not root edge: connect ancestor to internal edge (nextedge) */
	if (edge[nextedge].up != NULL) {
		if ( ((edge[nextedge].up)->downleft) == &edge[dockedge] )
			(edge[nextedge].up)->downleft  = &edge[nextedge];
		else
			(edge[nextedge].up)->downright = &edge[nextedge];
	}

	/* adding new external edge (nextedge+1) to the tree */
	edge[nextedge+1].up = &edge[nextedge];
	edge[nextedge+1].downleft = NULL;
	edge[nextedge+1].downright = NULL;
	edge[nextedge+1].taxon = permut[nextleaf];
	edgeofleaf[permut[nextleaf]] = nextedge+1;

	updatetreesplits_split(dockedge, permut[nextleaf], edge, Maxspc,
				edgeofleaf, nextedge, nextleaf, permut);

	(*in_nextedge) += 2;
	(*in_nextleaf) ++;

} /* addnextleaf */

/******************/


/* compute edge penalties using splits, if #minedges>1 choose one randomly */
void computepenalties_split(ONEEDGE  *tmpedge,        /* edge array           */
                            int       nextedge,       /* next free edge idx   */
                            int       Maxspc,         /* number of taxa       */
                            ulimatrix neighbormatr,   /* neighborhood matrix  */
                            int      *minedges,   /* minimum edge vector  */
                            int      *out_howmany,    /* number minimum edge  */
                            uli      *out_minpenalty, /* minimum penalty      */
                            int      *out_minedge)    /* minimum edge         */
{
	int  i;                /* counter */
	int  a, b;
	int  idxA, idxB;
	int  maxUP;            /* index counter for right/left split cluster */
	int  howmany  = 0;
	uli  minpenalty;

	for (i = 0; i < nextedge; i++) {
		(tmpedge[i]).edgeinfo = 0;		/* reset penalty */

		maxUP   = (tmpedge[i]).upsize;
		for (idxB = Maxspc - (tmpedge[i]).downsize; idxB < Maxspc; idxB++) {
			b = (tmpedge[i].split)[idxB];
			for (idxA = 0; idxA < maxUP; idxA++) {
				a = (tmpedge[i].split)[idxA];

				(tmpedge[i]).edgeinfo += neighbormatr[a][b];
			} /* for all root-sided (up) taxa */
		} /* for all non-root-sided (down) taxa */

		if (howmany == 0) { /* first edge: init */
			minedges[howmany++] = i;
			minpenalty = tmpedge[i].edgeinfo;
 		} else { /* -> all other edges */
			if (minpenalty == tmpedge[i].edgeinfo) { 
				/* another min edge */
				minedges[howmany++] = i;

			} else { /* -> not same minimum */

				if (minpenalty > tmpedge[i].edgeinfo) {
					/* new minimum penalty */
					howmany = 0;
					minedges[howmany++] = i;
					minpenalty = tmpedge[i].edgeinfo;
				} /* if new minimum */

			} /* same minimum */
		} /* if all other edges */

	} /* for all edges */

	*out_howmany    = howmany,
	*out_minpenalty = minpenalty;

	if (howmany > 1) 
		*out_minedge = minedges[randominteger(howmany)];
	else
		*out_minedge = minedges[0];

} /* computepenalties_split */

/******************/


/* trueID-ed (HAS) */
/* perform one single puzzling step to produce one intermediate tree */
void onepstep_split(                   /* PStep (intermediate) tree topol:   */
         ONEEDGE **edge,               /*   out: new array of edges          */
         int     **edgeofleaf,         /*   out: array of extern edge ptrs   */
         int      *rootleaf,           /*   out: root leaf in topol, starting perm[0]  */
         unsigned char *quartettopols, /* in: quartetblock with all topols   */
         int       Maxspc,             /* in: Number of species (n)          */
         ivector   permut)             /* in: species permutation (trueID)   */
{

	/* local variables: */
	int  Maxbrnch = (2*Maxspc)-3; /* Number of branches (2n-3)   */
	int  nextedge;                /* next free edge index (=3)   */
	int  nextleaf;                /* next free leaf index (=3)   */
	int  minedge;                 /* insertion edge of minimum penalty */
	uli  mininfo;                 /* minimum penalty   */
	int a, b, c, i;               /* quartet leaves, i to be inserted */
	int idxA, idxB;
	int idxC, idxI;               /* index counter for quartet leaves */
	int chooseX, chooseY;         /* end leaves of penalty path */
	ONEEDGE      *tmpedge;        /* new array of edges          */
	int          *tmpedgeofleaf;  /* array of extern edge ptrs   */
	int           tmprootleaf;    /* root edge                   */
	ulimatrix     neighbormatr;
	int          *minedges;
	int           howmany;

	minedges = (int *) calloc((size_t) Maxspc, sizeof(int));
	if (minedges == NULL) maerror("minimal edge set in onepstep_split");

	neighbormatr = new_ulimatrix(Maxspc,Maxspc);

	/* allocate and initialize new tree topology */
	inittree_split(&tmpedge, &tmpedgeofleaf, &tmprootleaf, Maxspc, Maxbrnch, &nextedge, &nextleaf, permut);

	for (idxI = 3; idxI < Maxspc; idxI++) { 

		i = permut[idxI];

		/* initialize penalty neighborhood matrix */
		for (idxA = 0; idxA < nextleaf - 1; idxA++) {
			a = permut[idxA];
			for (idxB = idxA + 1; idxB < nextleaf; idxB++) {
				b = permut[idxB];

				neighbormatr[a][b] = 0;
				neighbormatr[b][a] = 0;
			} 
		} /* for all entries in the upper neighbormatr */

		/*
		 * core of quartet puzzling algorithm
	 	 */

		for (idxA = 0; idxA < nextleaf - 2; idxA++) {
			a = permut[idxA];
			for (idxB = idxA + 1; idxB < nextleaf - 1; idxB++) {
				b = permut[idxB];
				for (idxC = idxB + 1; idxC < nextleaf; idxC++) {
					c = permut[idxC];

					/* check which two leaves out of
					   a,b,c are closer related to each
					   other than to leaf i according to a
					   least squares fit of the continous 
					   Bayesian weights to the seven
					   trivial "attractive regions". We 
					   assign a score of 1 to all edges 
					   on the penalty path between these 
					   two leaves chooseX and chooseY */

					/* de-trueID-ed (HAS) */
					checkquartet(a, b, c, i, &chooseX, &chooseY);
					(neighbormatr[chooseX][chooseY])++;

				} /* for all quartets q=(a,b,c,i): c */
			} /* for all quartets q=(a,b,c,i): b */
		} /* for all quartets q=(a,b,c,i): a */

		/* symetrize penalty neighborhood matrix */
		for (idxA = 0; idxA < nextleaf - 1; idxA++) {
			a = permut[idxA];
			for (idxB = idxA + 1; idxB < nextleaf; idxB++) {
				b = permut[idxB];

				neighbormatr[a][b] += neighbormatr[b][a];
				neighbormatr[b][a] = neighbormatr[a][b];
			} 
		} /* for all entries in the upper neighbormatr */

		computepenalties_split(tmpedge, nextedge, Maxspc, neighbormatr,
		                       minedges, &howmany, &mininfo, &minedge);

		/* add the next leaf on minedge */
		addnextleaf_split(minedge, tmpedge, tmpedgeofleaf, tmprootleaf, Maxspc, Maxbrnch, &nextedge, &nextleaf, permut);

	} /* adding all other leafs */

	*edge       = tmpedge;         /* export tree topology             */
	*edgeofleaf = tmpedgeofleaf;   /* export array of extern edge ptrs */
	*rootleaf   = tmprootleaf;     /* export root edge                 */

	free(minedges);
	free_ulimatrix(neighbormatr);
} /* onepstep_split */






