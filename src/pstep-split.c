/*
 *   pstep-split.c
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */

/* split based O(n^4) algorithm */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#define EXTERN extern

#include<stdio.h>
#include<stdlib.h>
#include"puzzle.h"
/* #include"util.h" */
#include"pstep-split.h"

#if PARALLEL
#	include "ppuzzle.h"
#endif


/******************************************************************************/
/* functions for representing and building puzzling step trees                */
/******************************************************************************/

/* initialize tree with the following starting configuration
 *
 *                2
 *         0  +------- C(=2)
 * A(=0) -----+
 *            +------- B(=1)
 *                1
 *
 *
 *               A(=0)
 *               [up       =NULL]
 *               [downleft =1]
 *               [downright=2]
 *               [cluster(up)=0]
 *               [cluster(down)=1,2]
 *               [upsize=1]
 *               [downsize=2]
 *
 *                    o
 *                    |
 *                    |
 *      +-------------+--------------+
 *      |                            |
 *      |                            |
 *      o                            o
 *
 *   C(=1)                        B(=2)
 *   [up       =0]                [up       =0]
 *   [downleft =NULL]             [downleft =NULL]
 *   [downright=NULL]             [downright=NULL]
 *   [cluster(up)=0,2]            [cluster(up)=0]
 *   [cluster(down)=1]            [cluster(down)=2]
 *   [upsize=2]                   [upsize=2]
 *   [downsize=1]                 [downsize=1]
 *
 *   nextedge = 3
 *   nextleaf = 3
 *   and set according edge maps
 *
 */
void inittree_split(ONEEDGE_SPLIT **edge, /* out: new array of edges          */
                   int     **edgeofleaf,/* out: array of external edge ptrs */
                   int       Maxspc,    /* in:  Number of species (n)       */
                   int       Maxbrnch,  /* in:  Number of branches (2n-3)   */
                   int      *nextedge,  /* out: next free edge index (=3)   */
                   int      *nextleaf)  /* out: next free leaf index (=3)   */
{
	int i;
	ONEEDGE_SPLIT *tmpedge;
	int          *tmpedgeofleaf;

	/* allocate the memory for the whole tree */

	/* allocate memory for vector with all the edges of the tree */
	tmpedge = (ONEEDGE_SPLIT *) calloc((size_t) Maxbrnch, sizeof(ONEEDGE_SPLIT) );
	if (tmpedge == NULL) maerror("edge in inittree");
	*edge = tmpedge;

	/* allocate memory for vetmpctor with edge numbers of leaves */
	tmpedgeofleaf = (int *) calloc((size_t) Maxspc, sizeof(int) );
	if (tmpedgeofleaf == NULL) maerror("edgeofleaf in inittree");
	*edgeofleaf = tmpedgeofleaf;

	/* allocate memory for all the edges the split map */
	for (i = 0; i < Maxbrnch; i++) {
		(tmpedge)[i].split = (int *) calloc((size_t) Maxspc, sizeof(int) );
		if (tmpedge[i].split == NULL) maerror("split array in inittree");
#if 0
		(tmpedge)[i].downcluster = (int *) calloc((size_t) Maxspc, sizeof(int) );
		if (tmpedge[i].downcluster == NULL) maerror("downcluster edgemap in inittree");

		/* old _orig algorithm */
		(tmpedge)[i].edgemap = (int *) calloc((size_t) Maxbrnch, sizeof(int) );
		if (tmpedge[i].edgemap == NULL) maerror("edgemap in inittree");
#endif
	}

	/* number all edges */
	for (i = 0; i < Maxbrnch; i++) tmpedge[i].numedge = i;

	/* initialize tree */

	*nextedge = 3;
	*nextleaf = 3;

#if 0
	/* edge maps */
	(tmpedge[0].edgemap)[0] = 0; /* you are on the right edge */
	(tmpedge[0].edgemap)[1] = 4; /* go down left for leaf 1 */
	(tmpedge[0].edgemap)[2] = 5; /* go down right for leaf 2 */
	(tmpedge[1].edgemap)[0] = 1; /* go up for leaf 0 */
	(tmpedge[1].edgemap)[1] = 0; /* you are on the right edge */
	(tmpedge[1].edgemap)[2] = 3; /* go up/down right for leaf 2 */
	(tmpedge[2].edgemap)[0] = 1; /* go up for leaf 0 */
	(tmpedge[2].edgemap)[1] = 2; /* go up/down left for leaf 1 */
	(tmpedge[2].edgemap)[2] = 0; /* you are on the right edge */
#endif

	(tmpedge[0].split)[0]        = 0; /* leaf 0 above this edge */
	 tmpedge[0].upsize           = 1;
	(tmpedge[0].split)[Maxspc-1] = 1; /* leaf 0 below this edge */
	(tmpedge[0].split)[Maxspc-2] = 2; /* leaf 0 below this edge */
	 tmpedge[0].downsize         = 2;

	(tmpedge[1].split)[0]        = 0; /* leaf 0 above this edge */
	(tmpedge[1].split)[1]        = 2; /* leaf 0 above this edge */
	 tmpedge[1].upsize           = 2;
	(tmpedge[1].split)[Maxspc-1] = 1; /* leaf 0 below this edge */
	 tmpedge[1].downsize         = 1;

	(tmpedge[2].split)[0]        = 0; /* leaf 0 above this edge */
	(tmpedge[2].split)[1]        = 1; /* leaf 0 above this edge */
	 tmpedge[2].upsize           = 2;
	(tmpedge[2].split)[Maxspc-1] = 2; /* leaf 0 below this edge */
	 tmpedge[2].downsize         = 1;


	/* interconnection */
	tmpedge[0].up        = NULL;
	tmpedge[0].downleft  = &tmpedge[1];
	tmpedge[0].downright = &tmpedge[2];

	tmpedge[1].up        = &tmpedge[0];
	tmpedge[1].downleft  = NULL;
	tmpedge[1].downright = NULL;

	tmpedge[2].up = &tmpedge[0];
	tmpedge[2].downleft  = NULL;
	tmpedge[2].downright = NULL;

	/* edges of leaves */
	tmpedgeofleaf[0] = 0;
	tmpedgeofleaf[1] = 1;
	tmpedgeofleaf[2] = 2;
} /* inittree */

/******************/
/* add next leaf on the specified edge */
void updatesplit_split(ONEEDGE_SPLIT curredge,    /* current edge                 */
                       int     wherefrom,   /* direction to last curr. edge */
                       int     newleaf,     /* leaf to add to splits        */
                       ONEEDGE_SPLIT *edge, /* edge array                   */
                       int     Maxspc,      /* No. of species               */
                       int     Maxbrnch,    /* No. of branches              */
                       int    *edgeofleaf,  /* ext. edge idx array          */
                       int    *in_nextedge, /* next free edge idx           */
                       int    *in_nextleaf) /* next free leaf idx           */
{
	int n;

	switch (wherefrom) {
		case STARTING:
			if (curredge.up != NULL) { /* non-root edge */
				curredge.split[Maxspc-(++(curredge.upsize))] = newleaf; /* start edge: must be down */
				if (((curredge.up)->downright) == &curredge) {
					updatesplit_split(*(curredge.up), DOWNRIGHT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				} else {
					updatesplit_split(*(curredge.up), DOWNLEFT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				}
			} else { /* root -> every thing below/shouldn't happen */
				for (n=0; n<*in_nextedge; n++) {
					edge[n].split[(edge[n].upsize)++] = newleaf;
				}
			}
			break;
		case UP:
			curredge.split[(curredge.downsize)++] = newleaf; /* leaf is up */
			if (curredge.downright != NULL) { /* not leaf */
				updatesplit_split(*(curredge.downright), UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				updatesplit_split(*(curredge.downleft), UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
			} else { /* nothing to do */
				return;
			}
			break;
		case DOWNLEFT:
			curredge.split[(curredge.downsize)++] = newleaf; /* leaf is down */
			if (curredge.downright != NULL) { /* not leaf */
				updatesplit_split(*(curredge.downright), UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
			}

			if (curredge.up != NULL) { /* not root */
				if (((curredge.up)->downright) == &curredge) {
					updatesplit_split(*(curredge.up), DOWNRIGHT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				} else {
					updatesplit_split(*(curredge.up), DOWNLEFT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				}
			}
			break;
		case DOWNRIGHT:
			curredge.split[(curredge.downsize)++] = newleaf; /* leaf is down */
			if ((curredge.downright) != NULL) { /* not leaf */
				updatesplit_split(*(curredge.downleft), UP, newleaf,
                       			edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
			}

			if ((curredge.up) != NULL) { /* not root */
				if (((curredge.up)->downright) == &curredge) {
					updatesplit_split(*(curredge.up), DOWNRIGHT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				} else {
					updatesplit_split(*(curredge.up), DOWNLEFT, newleaf,
                       				edge, Maxspc, Maxbrnch, edgeofleaf, in_nextedge, in_nextleaf);
				}
			}
			break;
	}
} /* addnextleaf_split */

/******************/
/* add next leaf on the specified edge */
void addnextleaf_split(int      dockedge,        /* insert here         */
                      ONEEDGE_SPLIT *edge,       /* edge array          */
                      int     *edgeofleaf,      /* ext. edge idx array */
                      int      Maxspc,          /* No. of species      */
                      int      Maxbrnch,        /* No. of branches     */
                      int     *in_nextedge,        /* next free edge idx  */
                      int     *in_nextleaf)        /* next free leaf idx  */
{
	int  i;
	int nextedge;
	int nextleaf;

	nextedge=*in_nextedge;
	nextleaf=*in_nextleaf;

	if (dockedge >= nextedge) {
		/* Trying to add leaf nextleaf to nonexisting edge dockedge */
		fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR F TO DEVELOPERS\n\n\n");
#		if PARALLEL
			PP_Finalize();
#		endif
		exit(1);
	}

	if (nextleaf >= Maxspc) {
		/* Trying to add leaf nextleaf to a tree with Maxspc leaves */
		fprintf(STDOUT, "\n\n\nHALT: PLEASE REPORT ERROR G TO DEVELOPERS\n\n\n");
#		if PARALLEL
			PP_Finalize();
#		endif
		exit(1);
	}

	/* necessary change in edgeofleaf if dockedge == edgeofleaf[0] */
	if (edgeofleaf[0] == dockedge) edgeofleaf[0] = nextedge;

	/* adding nextedge to the tree */
	edge[nextedge].up = edge[dockedge].up;
	edge[nextedge].downleft = &edge[dockedge];
	edge[nextedge].downright = &edge[nextedge+1];
	edge[dockedge].up = &edge[nextedge];

	if (edge[nextedge].up != NULL) {
		if ( ((edge[nextedge].up)->downleft) == &edge[dockedge] )
			(edge[nextedge].up)->downleft  = &edge[nextedge];
		else
			(edge[nextedge].up)->downright = &edge[nextedge];
	}

	/* adding nextedge + 1 to the tree */
	edge[nextedge+1].up = &edge[nextedge];
	edge[nextedge+1].downleft = NULL;
	edge[nextedge+1].downright = NULL;
	edgeofleaf[nextleaf] = nextedge+1;

#if 0
	/* the two new edges get info about the old edges */
	/* nextedge */
	for (i = 0; i < nextedge; i++) {
		switch ( (edge[dockedge].edgemap)[i] ) {

			/* down right changes to down left */
			case 5:	(edge[nextedge].edgemap)[i] = 4;
				break;

			/* null changes to down left */
			case 0:	(edge[nextedge].edgemap)[i] = 4;
				break;

			default:(edge[nextedge].edgemap)[i] = (edge[dockedge].edgemap)[i];
				break;
		}
	}

	/* nextedge + 1 */
	for (i = 0; i < nextedge; i++) {
		switch ( (edge[dockedge].edgemap)[i] ) {

			/* up/down left changes to up */
			case 2:	(edge[nextedge+1].edgemap)[i] = 1;
				break;

			/* up/down right changes to up */
			case 3:	(edge[nextedge+1].edgemap)[i] = 1;
				break;

			/* down left changes to up/down left */
			case 4:	(edge[nextedge+1].edgemap)[i] = 2;
				break;

			/* down right changes to up/down left */
			case 5:	(edge[nextedge+1].edgemap)[i] = 2;
				break;

			/* null changes to up/down left */
			case 0:	(edge[nextedge+1].edgemap)[i] = 2;
				break;

			/* up stays up */
			default:(edge[nextedge+1].edgemap)[i] =
							(edge[dockedge].edgemap)[i];
				break;
		}
	}

	/* dockedge */
	for (i = 0; i < nextedge; i++) {
		switch ( (edge[dockedge].edgemap)[i] ) {

			/* up/down right changes to up */
			case 3:	(edge[dockedge].edgemap)[i] = 1;
				break;

			/* up/down left changes to up */
			case 2:	(edge[dockedge].edgemap)[i] = 1;
				break;

			default:	break;
		}
	}

	/* all edgemaps are updated for the two new edges */
	/* nextedge */
	(edge[nextedge].edgemap)[nextedge] = 0;
	(edge[nextedge].edgemap)[nextedge+1] = 5; /* down right */

	/* nextedge + 1 */
	(edge[nextedge+1].edgemap)[nextedge] = 1; /* up */
	(edge[nextedge+1].edgemap)[nextedge+1] = 0;

	/* all other edges */
	for (i = 0; i < nextedge; i++) {
		(edge[i].edgemap)[nextedge] = (edge[i].edgemap)[dockedge];
		(edge[i].edgemap)[nextedge+1] = (edge[i].edgemap)[dockedge];
	}

	/* an extra for dockedge */
	(edge[dockedge].edgemap)[nextedge] = 1; /* up */
	(edge[dockedge].edgemap)[nextedge+1] = 3; /* up/down right */
#endif

	nextleaf++;
	nextedge = nextedge + 2;

	*in_nextedge=nextedge;
	*in_nextleaf=nextleaf;
} /* addnextleaf */


/******************/
/* free memory (to be called after inittree) */
void freetree_split(ONEEDGE_SPLIT *edge,          /* edge array          */
                   int     *edgeofleaf,         /* ext. edge idx array */
                   int      Maxspc)             /* No. of species      */
{
	int i;

	for (i = 0; i < 2 * Maxspc - 3; i++) free(edge[i].split);
	free(edge);
	free(edgeofleaf);
} /* freetree */


/******************/
/* trueID (HAS) */
/* writes OTU sitting on edge ed */
void writeOTU_split(FILE    *outfp,              /* output file          */
                   int      ed,                 /* edge to subtree      */
                   ONEEDGE_SPLIT *edge,          /* edge array           */
                   int     *edgeofleaf,         /* ext. edge idx array  */
                   int      nextedge,           /* next free edge idx   */
                   int      nextleaf,           /* next free leaf idx   */
                   int     *column,             /* current screen depth */
                   int     *trueID)             /* species permutation  */
{
	int i;

	/* test whether we are on a leaf */
	if (edge[ed].downright == NULL && edge[ed].downleft == NULL) {
		for (i = 1; i < nextleaf; i++) {
			if (edgeofleaf[i] == ed) { /* i is the leaf of ed */
				*column += fputid(outfp, trueID[i]);
				return;
			}
		}
	}

	/* we are NOT on a leaf */
	fprintf(outfp, "(");
	(*column)++;
	writeOTU_split(outfp, edge[ed].downleft->numedge, edge, edgeofleaf, nextedge, nextleaf, column, trueID);
	fprintf(outfp, ",");
	(*column)++;
	(*column)++;
	if (*column > 55) {
		*column = 2;
		fprintf(outfp, "\n  ");
	}
	writeOTU_split(outfp, edge[ed].downright->numedge, edge, edgeofleaf, nextedge, nextleaf, column, trueID);
	fprintf(outfp, ")");
	(*column)++;
} /* writeOTU */

/******************/
/* trueID (HAS) */
/* write tree */
void writetree_split(FILE   *outfp,              /* output file          */
                    ONEEDGE_SPLIT *edge,         /* edge array           */
                    int     *edgeofleaf,        /* ext. edge idx array  */
                    int      nextedge,          /* next free edge idx   */
                    int      nextleaf,          /* next free leaf idx   */
                    int     *trueID)            /* species permutation  */
{
	int column;

	column = 1;
	fprintf(outfp, "(");
	column += fputid(outfp, trueID[0]) + 3;
	fprintf(outfp, ",");
	writeOTU_split(outfp, edge[edgeofleaf[0]].downleft->numedge, edge, edgeofleaf, nextedge, nextleaf, &column, trueID);
	column++;
	column++;
	fprintf(outfp, ",");
	writeOTU_split(outfp, edge[edgeofleaf[0]].downright->numedge, edge, edgeofleaf, nextedge, nextleaf, &column, trueID);
	fprintf(outfp, ");\n");
} /* writetree */


/******************/
/* clear all edgeinfos */
void resetedgeinfo_split(ONEEDGE_SPLIT *edge,     /* edge array           */
                        int      nextedge)      /* next free edge idx   */
{
	int i;

	for (i = 0; i < nextedge; i++)
		edge[i].penalty = 0;
} /* resetedgeinfo */

#if 0
/******************/
/* increment all edgeinfo between leaf A and B */
void incrementedgeinfo_split(int      A,         /* start leaf of penalty path */
                            int      B,         /* start leaf of penalty path */
                            ONEEDGE_SPLIT *edge,   /* edge array           */
                            int     *edgeofleaf)  /* ext. edge idx array  */
{
	int curredge, finaledge, nextstep;

	if (A == B) return;

	finaledge = edgeofleaf[B];

	curredge = edgeofleaf[A];
	edge[curredge].penalty = edge[curredge].penalty + 1;

	while (curredge != finaledge) {
		nextstep = (edge[curredge].edgemap)[finaledge];
		switch (nextstep) {

			/* up */
			case 1:	curredge = (edge[curredge].up)->numedge;
				break;

			/* up/down left */
			case 2:	curredge = ((edge[curredge].up)->downleft)->numedge;
				break;

			/* up/down right */
			case 3:	curredge = ((edge[curredge].up)->downright)->numedge;
				break;

			/* down left */
			case 4:	curredge = (edge[curredge].downleft)->numedge;
				break;

			/* down right */
			case 5:	curredge = (edge[curredge].downright)->numedge;
				break;

		}
		edge[curredge].edgeinfo = edge[curredge].edgeinfo + 1;
	}
} /* incrementedgeinfo */
#endif


/******************/
/* checks which edge has the lowest edgeinfo
   if there are several edges with the same lowest edgeinfo,
   one of them will be selected randomly */
void minimumedgeinfo_split(ONEEDGE_SPLIT *edge,   /* edge array           */
                          int     *edgeofleaf,  /* ext. edge idx array  */
                          int      nextedge,    /* next free edge idx   */
                          int      nextleaf,    /* next free leaf idx   */
                          int     *out_minedge,     /* minimum edge set     */
                          uli     *out_mininfo)     /* minumum penalty      */
{
	int i, k, howmany, randomnum;
	int minedge;     /* minimum edge set     */
	uli mininfo;     /* minumum penalty      */

	howmany = 1;
	minedge = 0;
	mininfo = edge[0].penalty;
	for (i = 1; i < nextedge; i++)
		if (edge[i].penalty <= mininfo) {
			if (edge[i].penalty == mininfo) {
				howmany++;
			} else {
				minedge = i;
				mininfo = edge[i].penalty;
				howmany = 1;
			}
		}

	if (howmany > 1) { /* draw random edge */
		randomnum = randominteger(howmany) + 1; /* 1 to howmany */
		i = -1;
		for (k = 0; k < randomnum; k++) {
			do {
				i++;
			} while (edge[i].penalty != mininfo);
			minedge = i;
		}
	}

	*out_minedge=minedge;
	*out_mininfo=mininfo;
} /* minimumedgeinfo */





/*************************************************************************/
/*  global functions of the puzzling step                                */
/*************************************************************************/

/* trueID (HAS) */
/* perform one single puzzling step to produce one intermediate tree */
void onepstep_split(                         /* PStep (intermediate) tree topol: */
         ONEEDGE_SPLIT **edge,          /*   out: new array of edges        */
         int     **edgeofleaf,         /*   out: array of extern edge ptrs */
         unsigned char *quartettopols, /* in: quartetblock with all topols */
         int       Maxspc,             /* in: Number of species (n)        */
         ivector   permutation)        /* in: species permutation (trueID) */
{
	int  Maxbrnch = (2*Maxspc)-3;  /* Number of branches (2n-3)   */
	int  nextedge;                 /* next free edge index (=3)   */
	int  nextleaf;                 /* next free leaf index (=3)   */
	int  minedge;                  /* insertion edge of minimum penalty */
	uli  mininfo;                  /* minimum penalty   */
	int a, b, c, i;                /* quartet leaves, i to be inserted */
	int chooseX, chooseY;          /* end leaves of penalty path */

	/* allocate and initialize new tree topology */
	inittree_split(edge, edgeofleaf, Maxspc, Maxbrnch, &nextedge, &nextleaf);

	/* adding all other leafs */
	for (i = 3; i < Maxspc; i++) { 
					
		/* clear all edgeinfos */
		resetedgeinfo_split(*edge, nextedge);

		/*
		 * core of quartet puzzling algorithm
	 	 */

		for (a = 0; a < nextleaf - 2; a++)
			for (b = a + 1; b < nextleaf - 1; b++)
				for (c = b + 1; c < nextleaf; c++) {

					/* check which two leaves out of
					   a,b,c are closer related to each
					   other than to leaf i according to a
					   least squares fit of the continous 
					   Bayesian weights to the seven
					   trivial "attractive regions". We 
					   assign a score of 1 to all edges 
					   on the penalty path between these 
					   two leaves chooseX and chooseY */

					/* trueID (HAS) */
					checkquartet_trueID(a, b, c, i, permutation, &chooseX, &chooseY);
					incrementedgeinfo_split(chooseX, chooseY, *edge, *edgeofleaf);

		} /* for all quartets q=(a,b,c,i) */

		/* find out which edge has lowest edgeinfo */
		minimumedgeinfo_split(*edge, *edgeofleaf, nextedge, nextleaf, &minedge, &mininfo);

		/* add the next leaf on minedge */
		addnextleaf_split(minedge, *edge, *edgeofleaf, Maxspc, Maxbrnch, &nextedge, &nextleaf);
	} /* adding all other leafs */
} /* onepstep_split */



/*************************************************************************/

/* perform Numtrial single puzzling steps constructing Numtrial intermediate */
/* trees, sort each of them and extract splits for consensus step            */
void allpstep_split(uli       Numtrial,         /* in: no. psteps to perform       */
              unsigned char *quartetinfo, /* in: quartetblock with all topols*/
              int       Maxspc,           /* in: Number of species (n)       */
              int       fixedorder_optn)  /* in: 'fixed' anchored RNG (debug)*/
{
	/* misc variables */
	/* trueID (HAS) */
	ivector       trueIDtmp;       /* species permutation (trueID)     */
	uli           Currtrial;       /* step counter                     */

	/* PStep (intermediate) tree topol: */
	ONEEDGE_SPLIT *tmpedgeset;      /*   array of edges                 */
	int          *tmpedgeofleaf;   /*   array of extern edge pointers  */

	/* for unique sorting of tree topologies */
	int  *ctree;		/* sort tree */
	int   startnode;	/* root in sort tree */
	char *trstr;		/* phylip tree string */
	treelistitemtype *treeitem; /* list entry of tree */

#	if PARALLEL
		cmatrix *bp;	/* temporary buffer for splits in slaves */
		int n;		/* step count for init/free split buffer */
#	endif /* PARALLEL */


#	if PARALLEL
		/* add times to arry */
		addtimes(GENERAL, &tarr);

		/* alloc temporary split memory */
		bp = (cmatrix *) calloc((size_t) Numtrial, sizeof(void *));
		for(n=0; n<Numtrial; n++) {
			bp[n] = new_cmatrix(Maxspc - 3, Maxspc);
		}
#	endif /* PARALLEL */


	/* do single puzzling steps Numtrial times */
	trueIDtmp = new_ivector(Maxspc);
	for (Currtrial = 0; Currtrial < Numtrial; Currtrial++) {
		/* randomize input order */
		if (fixedorder_optn) {
			fixedchooser(Maxspc, Maxspc, trueIDtmp);
		} else {
			if (! readsubset_optn) {
				chooser(Maxspc, Maxspc, trueIDtmp);
			} else { /* readsubset_optn */
				permutetaxa_ss(Maxspc, trueIDtmp, Namestr, 
				               Maxsubset, ss_setovlgraph, 
				               ss_setoverlaps, ss_setovllist, 
				               ss_setovllistsize, ss_matrix, 
				               ss_list, ss_listsize);
			} /* else readsubset_optn */
		}

		/* one single puzzling step */
		onepstep_split(&tmpedgeset, &tmpedgeofleaf, quartetinfo, Maxspc, trueIDtmp); 

		/* compute bipartitions of current tree */
		/* missing parameters (HAS) */
#		if ! PARALLEL	
			computebiparts_trueID(tmpedgeset, tmpedgeofleaf, trueIDtmp, 
			               biparts, outgroup, Maxspc); 

			makenewsplitentries(biparts, Maxspc-3, 
			                    &splitpatterns, &splitsizes, 
			                    &splitfreqs, &numbiparts, 
			                    &maxbiparts, Maxspc);
#		else /* PARALLEL */
			computebiparts_trueID(tmpedgeset, tmpedgeofleaf, trueIDtmp, 
			               bp[Currtrial], outgroup, Maxspc); 

			/* file splits not (yet) needed in slaves */
			/* filing is done centrally at the master */
			/* maybe optimizable by presorting (HAS) */
#			if 0
				makenewsplitentries(bp[Currtrial], Maxspc-3, 
				                    &splitpatterns, &splitsizes,
				                    &splitfreqs, &numbiparts, 
				                    &maxbiparts, Maxspc);
#			endif
#		endif /* else PARALLEL */


		/* Sorting to find unique topologies:               */
		/****************************************************/
		/* sort tree topology branches, so that within      */
		/* each pair of sister subtrees the right-most      */
		/* leaf's ID of the right subtree is smaller than   */
		/* the right-most leaf's ID of the lest subtree.    */
		/* Exception: The outgroup (root) is the tree's     */
		/*            right-most leaf!                      */

		/* init/alloc copy tree structure */
		ctree = initctree();
		/* copy tree structure */
		copytree_trueID(ctree, trueIDtmp, tmpedgeset, tmpedgeofleaf, Maxspc); 
		/* sort tree, startnode=outgroup */
		startnode = sortctree(ctree);
		/* sorted tree -> phylip tree string */
		trstr=sprintfctree(ctree, psteptreestrlen);

		/* add sorted tree to unique tree list */
		treeitem = addtree2list(&trstr, 1, &psteptreelist, &psteptreenum, &psteptreesum);

#		if ! PARALLEL
			/* output unique tree to trace list, if option set */
			/* not done on slave processes */
			if((listqptrees == PSTOUT_LIST) || (listqptrees == PSTOUT_LISTORDER)) {
				/* print: order no/# topol per this id/tree id/sum of unique topologies/sum of trees so far */
				fprintf(qptlist, "%ld.\t1\t%d\t%d\t%d\t%d\n", 
					Currtrial + 1, (*treeitem).count, (*treeitem).id, psteptreenum, psteptreesum);
			}
#		endif /* ! PARALLEL */

#		ifdef VERBOSE1
			printf("%s\n", trstr);
			printfsortedpstrees(psteptreelist);
#		endif

		/* free sorting tree structure */
		freectree(&ctree);

		/* free tree before building the next tree */
		freetree_split(tmpedgeset, tmpedgeofleaf, Maxspc);

#		if ! PARALLEL
		/* generate message roughly every 15 minutes */
		/* check timer */
		time(&time2);
		if ( (time2 - time1) > TIMECHECK_INTERVAL) {
			double tc2, mintogo, minutes, hours;
			/* every TIMECHECK_INTERVAL seconds */
			/* percentage of completed trees */
			if (mflag == 0) {
				fprintf(STDOUT, "\n");
				mflag = 1;
			}
			tc2 = 100.0*Currtrial/Numtrial;
			/* excluded for less runtime, but less accuracy of the 15min */
			/* + 100.0*nq/Numquartets/Numtrial; */
			mintogo = (100.0-tc2) *
				(double) (time2-time0)/60.0/tc2;
			hours = floor(mintogo/60.0);
			minutes = mintogo - 60.0*hours;
			fprintf(STDOUT, "%2.2f%%", tc2);
			fprintf(STDOUT, " completed (remaining");
			fprintf(STDOUT, " time: %.0f", hours);
			fprintf(STDOUT, " hours %.0f", minutes);
			fprintf(STDOUT, " minutes)\n");
			fflush(STDOUT);
			time1 = time2;
		} /* check timer */
#		endif /* ! PARALLEL */

		addtimes(PUZZLING, &tarr);
	} /* for Numtrials */

#	if PARALLEL
		/* in slaves: send results: splits, trees */
		PP_SendSplitsBlock(Maxspc, Numtrial, bp, psteptreenum, psteptreelist);

		/* free temporary split memory */
		for (Currtrial = 0; Currtrial < Numtrial; Currtrial++) {
			free_cmatrix(bp[Currtrial]);
		}
		free(bp);

		/* sent! Thus, in slave process not needed any more */
		freetreelist(&psteptreelist, &psteptreenum, &psteptreesum);
#	endif /* PARALLEL */

} /* allpstep_split */


