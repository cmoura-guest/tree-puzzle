/*
 * consensus.h
 *
 *
 * Part of TREE-PUZZLE 5.2 (July 2004)
 *
 * (c) 2003-2004 by Heiko A. Schmidt, Korbinian Strimmer, and Arndt von Haeseler
 * (c) 1999-2003 by Heiko A. Schmidt, Korbinian Strimmer,
 *                  M. Vingron, and Arndt von Haeseler
 * (c) 1995-1999 by Korbinian Strimmer and Arndt von Haeseler
 *
 * All parts of the source except where indicated are distributed under
 * the GNU public licence.  See http://www.opensource.org for details.
 *
 * ($Id$)
 *
 */


#ifndef CONSENSUS_H
#define CONSENSUS_H

#include "puzzle.h"
#include "util.h"

typedef struct {
	uli fullres_pro;
	uli fullres_con;
	uli partres_pro;
	uli partres_con;
	uli unres;
	uli missing;
	uli qsum;
} qsupportarr_t;

EXTERN cmatrix biparts;      /* bipartitions of tree of current puzzling step */
EXTERN cmatrix consbiparts;  /* bipartitions of majority rule consensus tree */

EXTERN int xsize;            /* depth of consensus tree picture */
EXTERN ivector consconfid;   /* confidence values of majority rule consensus tree */
EXTERN ivector conssizes;    /* partition sizes of majority rule consensus tree */
EXTERN ivector xcor;         /* x-coordinates of consensus tree nodes */
EXTERN ivector ycor;         /* y-coordinates of consensus tree nodes */
EXTERN ivector ycormax;      /* maximal y-coordinates of consensus tree nodes */
EXTERN ivector ycormin;      /* minimal y-coordinates of consensus tree nodes */

/* splits for consensus */
EXTERN int splitlength;      /* length of one entry in splitpatterns */
EXTERN uli maxbiparts;       /* memory reserved for maxbiparts bipartitions */
EXTERN uli numbiparts;       /* number of different bipartitions */

EXTERN int *splitsizes;      /* size of all different splits of all trees */
EXTERN uli *splitcomptemp;   /* temp variable to compress bipartitions coding */
EXTERN uli *splitfreqs;      /* frequencies of different splits of all trees */
EXTERN uli *splitpatterns;   /* all different splits of all trees */

EXTERN qsupportarr_t *qsupportarr; /* quartet support values per split */

EXTERN int consincluded;     /* number of included biparts in the consensus tree */
EXTERN int consfifty;        /* number of biparts >= 50% */
EXTERN int conscongruent;    /* number of first incongruent bipart */

/******************************************************************************/
/* functions for computing the consensus tree                                 */
/******************************************************************************/

/* prepare for consensus tree analysis */
void initconsensus();

/* recursive function to get bipartitions */
/* traversal should be optimazable (HAS) */
void makepart_trueID(int           i,          /*  */
              int           curribrnch, /* in: current branch in traversal */
              ONEEDGE      *edge,       /* in: tree topology    */
              int          *edgeofleaf, /* in: ext. edge list   */
              int          *trueID,     /* in: permutation list */
              cmatrix       biparts,    /* out: split strings, edge by edge */
              int           Maxspc);	/* in: number of species in tree */

/* compute bipartitions of current puzzling step tree */
void computebiparts(ONEEDGE      *edge,       /* in: tree topology          */
                    int          *edgeofleaf, /* in: ext. edge list         */
                    int           psteprootleaf, /* virtual root from pstep */
                    cmatrix       biparts,    /* out: splits                */
                    int           outgroup,   /* in: outgroup of tree       */
                    int           Maxspc);    /* in: number of taxa in tree */

/* print out the bipartition n of all different splitpatterns */
void fprintfsplit(FILE *fp,		/* outputfile stream */
                uli   n,		/* split number */
                uli  *splitpatterns,	/* splits */
                int   splitlength,	/* number of ulis per split */
                int   Maxspc);		/* number of taxa */

/* make new entries for new different bipartitions and count frequencies */
/* internal use: splitcomptemp */
void makenewsplitentries(cmatrix  bip,             /* in: split string vector */
                         int      numspl,          /* in: no. of new splits   */
                         uli    **in_splitpatterns,/* io: known compr splits  */
                         int    **in_splitsizes,   /* io: kn. split sizes: '.'*/
                         uli    **in_splitfreqs,   /* io: kn. split frequences*/
                         uli     *in_numbiparts,   /* io: no. of splits so far*/
	                 uli     *in_maxbiparts,   /* io: reserved memory     */
                         int      Maxspc);         /* in: no. of species      */

/***************************************************************************/

/* copy bipartition n of all different splitpatterns to consbiparts[k] */
void copysplit(uli n, uli *splitpatterns, int k, cmatrix consbipartsvec);

/* <consbipartsvec> and the quartet topologies from the ML step. Values are  */
/* stored in <qsupparr[splitnum]>                                            */
/* missing parameter: quartetinfo */
void quartsupport(int splitnum, cmatrix consbipartsvec, qsupportarr_t *qsupparr);

/* compute majority rule consensus tree */
void makeconsensus(uli maxnumtrees, int qsupp_optn);

/* write node (writeconsensustree) */
/* missing: column */
void writenode(FILE          *treefile,    /* in: output stream */
               int            node,        /* current node */
               int            qsupp_optn,  /* 'print quartet support' flag */
               qsupportarr_t *qsupparr,    /* quartet support values */
               int           *column);     /* current line position */

/* write consensus tree */
void writeconsensustree(FILE          *treefile,   /* in: output stream */
                        int            qsupp_optn, /* 'print quartsupp' flag */
                        qsupportarr_t *qsupparr);  /* quartet support values */

/* establish node coordinates (plotconsensustree) */
void nodecoordinates(int node);

/* drawnode  (plotconsensustree) */
void drawnode(int node, int xold);

/* plot consensus tree */
void plotconsensustree(FILE *plotfp);


#endif /* CONSENSUS_H */

